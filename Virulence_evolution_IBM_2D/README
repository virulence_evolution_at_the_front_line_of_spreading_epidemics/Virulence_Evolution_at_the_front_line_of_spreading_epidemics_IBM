
    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

    
-------------------------------------

This is a program I wrote without considering that it would be one day released; thus I made almost no effort to make it usable by someone else. You will notice that the code is poorly commented and uses a lot of my own routines, which is a very bad idea in general (and in particular when it is released). However, if you find it useful, here are some basic comments that will help you use and understand the code. 

You should use the script compile.sh to make an executable out of the sources. Notice that I have never tested it on any other operating system than Linux (my OS of choice is Debian), and therefore I don't even know if it will compile on Windows or Mac OS. In any case, I noticed that turning on the optimization flags sensibly improves the execution speed - which is not the case in the script. 

You should try the --help option on the command line. It will give you a basic summary of the commands and the list of the variables you can use to interact with the program. Interaction is done exclusively with command-line options with the convention key=value for variables and --foo for flags. Example : 
	./Virulence_evolution_IBM_2D --sample sample_every=0.01 T=100
The name of the variables should be self-explanatory. If in doubt, try reverse-engeneering the code to find out the purpose of a variable. There are a few variables that could be hard to figure out : 
   * N is the total number of hosts per site
   * m,n is the size of the rectangle (matrix) used by the program. m is the number of columns, n is the number of lines.
   * T is the final time of the simulation
   * mbeta, wbeta are the transmission rate of the mutant and wild type
   * mb, wd are the death rate of the mutant and wild type
   * etc...
   
You should give an initial condition to the program using the file_init_cond variable. If not, the simulation starts from an initial population of 0, so you should not see anything happen. I have joined a script GenInitCond.py that can help you generate such a file. Notice that it requires Python 3 to work. 
   
Output is strictly ascii text. There are two kind of formatting available : CSV and XML. The first lines of output is a header with the options that are in the memory at runtime; that way, you can always retrieve the original parameters. In both cases, the mutant an wild type states are given as space-separated arrays. If you wish to use this data in a plot, I recommand that you format is as you wish using an external scripting language as python and then use a plotting program (I sometimes use gnuplot, or draw it directly with LaTeX). In particular, there is not 2D structure on those arrays : the output is striclty a concatenation of the lines. So you have to know the number of columns to make sense of the data.

I would like to apologize in advance in the case an experimentalist finds the need to read the code. I recognize now that computer simulations should not be called "Experiments", but I wasn't aware of this issue by the time I wrote the code and I needed to call the class somehow. I hope there is no harm done. 

There is basically two sides in the code : a "generic" part that deals with user interaction and storage, and an "ad hoc" part that does the real computation. The center of the program is the class Experiment in the files Experiment.h and Experiment.cpp, so it would be a good place to start if you want to modify the core of the simulation. It makes everything work together. 

