/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Experiment.cpp                                                             *
* Experience de diffusion-mutation avec deux types                           *
*****************************************************************************/

#include "Experiment.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Experiment::Experiment(double Lambda, double wBeta, double wD, double wm, double mBeta, double mD, double mm, size_t Nx, size_t My, size_t Nmax, double Tf)
	:Engine_event_XMLsample_base(0, "", false, false, false, "", false, false, false, "csv"), pheno(NULL), lambda(Lambda), wbeta(wBeta), wd(wD), wmu(wm), mbeta(mBeta), md(mD), mmu(mm), T(Tf), n(Nx), m(My), N(Nmax), file_init_cond(""), extra_verbose(false)
{
	// Parametres du modele
	Add("lambda", lambda);
	Add("wbeta", wbeta);
	Add("mbeta", mbeta);
	Add("N", N);
	Add("wd", wd);
	Add("md", md);
	Add("wmu", wmu);
	Add("mmu", mmu);
	Add("n", n);
	Add("m", m);
	Add("T", T);
		
	// Parametres du programme
	Add("file_init_cond", file_init_cond);
	
	// Commandes
	Set("--extra_verbose", "tres bavard", extra_verbose);
}

Experiment::~Experiment()
{
	if (pheno != NULL)
		delete pheno;
}

/*****************************************************************************
* Traite les messages                                                        *
*****************************************************************************/

bool Experiment::HandleEvent(size_t ev)
{
	switch(ev)
	{
		// Initialisation
		case EVENT_INIT:
		{
			// Recuperation des parametres du modele
			Get("lambda", lambda);
			Get("wbeta", wbeta);
			Get("mbeta", mbeta);
			Get("N", N);
			Get("wd", wd);
			Get("md", md);
			Get("wmu", wmu);
			Get("mmu", mmu);
			Get("n", n);
			Get("m", m);
			Get("T", T);
						
			// Recuperation des parametres du programme
			Get("file_init_cond", file_init_cond);
			
			// Recuperation des commandes
			GetCmd("--extra_verbose", extra_verbose);
			
			// On definit la date de fin du programme
			evman.Push_Event(T, EVENT_EXIT);
						
			// Initial population
									
			vector<size_t> winit_cond(n*m, 0);
			vector<size_t> minit_cond(n*m, 0);
			
			if (file_init_cond.size()==0)
			{
				for (size_t i=0; i<n; i++)
				{
					for (size_t j=0; j<m; j++)
						winit_cond[m*i+j]=((i==n/2) && (j==m/2)) ? N:0;
				}
			}
			else
			{
				SimpleXMLTree * InitCond=new SimpleXMLTree("root");
				
				InitCond->Parse(file_init_cond);
				
				InitCond->CleanChildren(true);
	
				size_t type_pheno=0;
				
				type_pheno=InitCond->find("wildtype");
				winit_cond=GetInitCond(InitCond->GetChild(type_pheno)->GetText());

				type_pheno=InitCond->find("mutant");
				minit_cond=GetInitCond(InitCond->GetChild(type_pheno)->GetText());
				
				winit_cond.resize(m*n, 0);
				minit_cond.resize(m*n, 0);
				
				delete InitCond;
			}			

			lambda=lambda/N;
										
			pheno=new Modele_epidemio_adhoc(winit_cond, minit_cond, N, n, m);
			
			// On initialise le moteur de Gillespie
			G=Gillespie(9);
			
			G.Set(0, pheno->GetPop(ID_WILDTYPE)*wd); 	// Wild type death
			G.Set(1, pheno->GetPop(ID_MUTANT)*md); 		// Mutant death
			G.Set(2, wbeta*pheno->GetNewInfections(ID_WILDTYPE)/N);	// new wild type
			G.Set(3, mbeta*pheno->GetNewInfections(ID_MUTANT)/N);	// new mutant
			G.Set(4, pheno->GetPop(ID_WILDTYPE)*wmu); 	// Wild type mutation
			G.Set(5, pheno->GetPop(ID_MUTANT)*mmu); 		// Mutant mutation
			G.Set(6, pheno->GetTotalCouples(ID_WILDTYPE)*lambda); // Wild type move
			G.Set(7, pheno->GetTotalCouples(ID_MUTANT)*lambda); // Wild type move
			G.Set(8, pheno->GetExchange()*lambda);
			G.Update();
			
			// Envoi du premier evenement
			evman.Push_Event(G.GetNextTime(), EVENT_GILLESPIE);
			
			if (extra_verbose)
			{
				cout << "init terminated" << endl;
				cout << "pop : " << pheno->GetPop(ID_WILDTYPE) + pheno->GetPop(ID_MUTANT) << endl;
			}
		}
		break;
			
		case EVENT_GILLESPIE:
		{
			if (extra_verbose)
			{
				cout << "time : " << evman.GetTime() << endl;
				cout << "pop : " << pheno->GetPop(ID_WILDTYPE) + pheno->GetPop(ID_MUTANT) << endl;
			}
			
			// On selectionne l'evenement sur la population
			switch (G.GetNextEvent())
			{
				case 0: // KILL wild type
					if (extra_verbose)
						cout << "KILL WT" << endl;
					// pheno gere en interne la repartition
					if (!pheno->Kill(ID_WILDTYPE))
						cerr << evman.GetTime() << " : KILL WT failed " << endl;
					break;
				
				case 1: // KILL mutant
					if (extra_verbose)
						cout << "KILL M" << endl;
					// pheno gere en interne la repartition
					if (!pheno->Kill(ID_MUTANT))
						cerr << evman.GetTime() << " : KILL M failed " << endl;
					break;
					
				case 2: // NEW_INF wild type
					if (extra_verbose)
						cout << "NEW_INF WT" << endl;
						
					if (!pheno->NewInfection(ID_WILDTYPE))
						cerr << evman.GetTime() << " : NEW_INF WT failed" << endl;
					break;
					
				case 3: // NEW_INF wild type
					if (extra_verbose)
						cout << "NEW_INF M" << endl;
						
					if (!pheno->NewInfection(ID_MUTANT))
						cerr << evman.GetTime() << " : NEW_INF M failed" << endl;
					break;	
				case 4: // MUT wild_type
					if (extra_verbose)
						cout << "MUT WT" << endl;
						
					if (!pheno->Mutate(ID_WILDTYPE))
						cerr << evman.GetTime() << " : MUT failed" << endl;
						
					break; 
					
				case 5: // MUT mutant
					if (extra_verbose)
						cout << "MUT M" << endl;
						
					if (!pheno->Mutate(ID_MUTANT))
						cerr << evman.GetTime() << " : MUT failed" << endl;
					break;
					
				case 6: // MOVE wild type
					if (extra_verbose)
						cout << "MOVE WT" << endl;
						
					if (!pheno->Move(ID_WILDTYPE))
						cerr << evman.GetTime() << " : MOVE WT failed " << endl;
					break;
					
				case 7: // MOVE mutant
					if (extra_verbose)
						cout << "MOVE M" << endl;
						
					if (!pheno->Move(ID_MUTANT))
						cerr << evman.GetTime() << " : MOVE M failed " << endl;
					break;
					
				case 8: // MOVE mutant
					if (extra_verbose)
						cout << "EXCHANGE" << endl;
						
					if (!pheno->Exchange())
						cerr << evman.GetTime() << " : EXCHANGE failed " << endl;
					break;
			}
			
			pheno->All();
			
			if (pheno->GetPop(ID_WILDTYPE)+pheno->GetPop(ID_MUTANT) > 0)
			{
				// Mise a jour des evenements
		
				G.Set(0, pheno->GetPop(ID_WILDTYPE)*wd); 	// Wild type death
				G.Set(1, pheno->GetPop(ID_MUTANT)*md); 		// Mutant death
				G.Set(2, wbeta*pheno->GetNewInfections(ID_WILDTYPE)/N);	// new wild type
				G.Set(3, mbeta*pheno->GetNewInfections(ID_MUTANT)/N);	// new mutant
				G.Set(4, pheno->GetPop(ID_WILDTYPE)*wmu); 	// Wild type mutation
				G.Set(5, pheno->GetPop(ID_MUTANT)*mmu); 		// Mutant mutation
				G.Set(6, pheno->GetTotalCouples(ID_WILDTYPE)*lambda); // Wild type move
				G.Set(7, pheno->GetTotalCouples(ID_MUTANT)*lambda); // mutant move
				G.Set(8, pheno->GetExchange()*lambda);
			
				// Recalcul de la somme des taux pour eviter les erreurs d'arrondi
				G.Update();
								
				// Envoi de l'evenement suivant
				evman.Push_Event(evman.GetTime() + G.GetNextTime(), EVENT_GILLESPIE);
			}
		}
		break;
		
		case EVENT_AT_EXIT:
		{
			// Nothing to do
		}
		break;
		
		default:
			cerr << "Unexpected event" << endl;
			return false;
	}
	
	return true;
}

/*****************************************************************************
* Initialise les echantillons                                                *
*****************************************************************************/

bool Experiment::SampleInit(SimpleXMLTree & temp, SimpleXMLTree & tp)
{
		
	GetVarXML(*&tp);	
	
	// Creates template of sample
	temp.Append(new SimpleXMLTree("time", "", 0));
		
	temp.Append(new SimpleXMLTree("wildtype", "", 0));

	temp.Append(new SimpleXMLTree("mutant", "", 0));
	
	return true;
}

/*****************************************************************************
* Echantillonnage                                                            *
*****************************************************************************/

bool Experiment::Sample(SimpleXMLTree & tp)
{
	ostringstream txt;
	string str_txt;
	
	txt << evman.GetTime();
	str_txt=txt.str();
	
	txt.str("");
	
	// sample time
	tp.GetChild(tp.find("time"))->GetText()=str_txt;
	
	// sample the wild type
	
	str_txt="";
	
	for (size_t i=0; i<pheno->operator[](ID_WILDTYPE).size(); i++)
	{
		string tmpstr;
		
		txt << pheno->operator[](ID_WILDTYPE)[i];
		tmpstr=txt.str();
		
		txt.str("");
		
		str_txt+=tmpstr + " ";
	}
	
	tp.GetChild(tp.find("wildtype"))->GetText()=str_txt;
	

	// sample the mutant
	
	str_txt="";
	
	for (size_t i=0; i<pheno->operator[](ID_MUTANT).size(); i++)
	{
		string tmpstr;
		
		txt << pheno->operator[](ID_MUTANT)[i];
		tmpstr=txt.str();
		
		txt.str("");
		
		str_txt+=tmpstr + " ";
	}
	
	tp.GetChild(tp.find("mutant"))->GetText()=str_txt;
	
	return true;
}

/*****************************************************************************
* Echantillonnage final                                                      *
*****************************************************************************/

bool Experiment::SampleAtExit(SimpleXMLTree & tp)
{
	bool ret=true;
	
	if (sample_at_exit)
		ret &= Sample(tp);
	
	return ret;
}

