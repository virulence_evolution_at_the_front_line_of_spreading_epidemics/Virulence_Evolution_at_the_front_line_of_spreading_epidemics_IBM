/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Modele_epidemio_adhoc.cpp                                                  *
* Self-explaining : this model is optimized for one purppse only             *
* This one codes a SI model at population equilibrium                        *
* note that the susceptible hosts are implicit (I+S=N)                       *
*****************************************************************************/

#include "Modele_epidemio_adhoc.h"

Modele_epidemio_adhoc::Modele_epidemio_adhoc(const vector<size_t> & winit, const vector<size_t> & minit, size_t pop_local, size_t nlines, size_t ncolumns)
	:Local_counter_bank<size_t, 2, size_t>(vector<size_t>(winit.size(), 0)), N(pop_local), m(ncolumns), n(nlines), wpop(ID_WILDTYPE), mpop(ID_MUTANT), wsquares(ID_WILDTYPE), msquares(ID_MUTANT), wmove(ID_WILDTYPE, ID_MUTANT, n, m, N), mmove(ID_MUTANT, ID_WILDTYPE, n, m, N), scalarp(ID_WILDTYPE, ID_MUTANT), exc(ID_WILDTYPE, ID_MUTANT, n, m)
{
	AddPayoff(&wpop);
	AddPayoff(&mpop);
	AddPayoff(&wsquares);
	AddPayoff(&msquares);
	AddPayoff(&wmove);
	AddPayoff(&mmove);
	AddPayoff(&scalarp);
	AddPayoff(&exc);
	
	for (size_t i=0; i<winit.size(); i++)
	{
		this->operator()(ID_WILDTYPE, i)=winit[i];
		Next();
	}
	
	for (size_t i=0; i<minit.size(); i++)
	{
		this->operator()(ID_MUTANT, i)=minit[i];
		Next();
	}
	
	
	All(); 			// Empty the stack, just to be sure
}		
	
bool Modele_epidemio_adhoc::Kill(size_t who) 	// Kill one wild type or one mutant, depending on
					// variable who
{
	size_t x=MyAlea::a.Uniform(GetPop(who)) + 1;
	size_t i=0, sum=0;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i++];
	}
	
	i--;
	
	if (this->operator[](who)[i]>0)
	{
		this->operator()(who, i)=this->operator[](who)[i]-1; 	// Schedule reduction in population
		
		return true;
	}
	else
		return false;
}

bool Modele_epidemio_adhoc::Mutate(size_t who) 	// Kill one wild type or one mutant, depending on
						// variable who
{
	size_t x=MyAlea::a.Uniform(GetPayValue(ID_RES_POP+who)) + 1;
	size_t i=0, sum=0;
	size_t the_other=(who+1) % 2;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i++];
	}
	
	i--;
	
	if ((this->operator[](who)[i]>0)&&(this->operator[](the_other)[i]<N))
	{
		this->operator()(who, i)=this->operator[](who)[i]-1; 	// Schedule reduction in population
		this->operator()(the_other, i)=this->operator[](the_other)[i]+1; 	// Schedule increase in population
		
		return true;
	}
	else
		return false;
}

bool Modele_epidemio_adhoc::Exchange()	// move from one column to another with uniform probability on
					// (infected(i) \times susceptible(i+inc))
{
	size_t total=GetExchange();
	size_t x=MyAlea::a.Uniform(total)+1;
	size_t who=ID_WILDTYPE;
	size_t the_other=ID_MUTANT;
	size_t i=0, j=0, sum=0;
	size_t inc=0;
		
	size_t line=0; 
	size_t column=0;

	// Determine where the exchange takes place
	while (x > sum)
	{
		line=i/m; 
		column=i%m;
		inc=0;
		
		switch(j%4)
		{
			case 0:
				if (column<m-1)
					inc=this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i+1]; 
				break;
			
			case 1:
				if (line<n-1)
					inc=this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i+m];
				break;
			
			case 2:
				if (column>0)
					inc=this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i-1];
				break;
			
			case 3:
				if (line>0)
					inc=this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i-m];
				break;
		}
		
		sum+=inc;
		j++;
		if ((j%4)==0)
		{
			i++;
		}		
	}
	
	j--;
	if ((j%4)==3)
		i--;
		
	long target=0;
	switch (j%4)
	{
		case 0:
			target=1;
			break;
		case 1:
			target=m;
			break;
		case 2:
			target=-1;
			break;
		case 3: 
			target=-m;
			break;
	}
		
	line=i/m; 
	column=i%m;
	
	sum -= inc;
	
	// Makes the transaction
	bool good=true;
	
	if (this->operator[](who)[i]>0)
	{
		// Execute who's transaction (with check)
		this->operator()(who, i)=this->operator[](who)[i]-1;

		if (this->operator[](who)[i+target]<N)
		{
			this->operator()(who, i+target)=this->operator[](who)[i+target]+1;
			
			if (this->operator[](the_other)[i+target]>0)
			{
				// Execute the other's transaction (with check)
				this->operator()(the_other, i+target)=this->operator[](the_other)[i+target]-1;

				if (this->operator[](the_other)[i]<N)
				{
					this->operator()(the_other, i)=this->operator[](the_other)[i]+1;
				}
				else
					good=false;
			
				if (!good)
					this->operator()(the_other, i+target)=this->operator[](the_other)[i+target];
			}
			else
			{
				good=false;
			}
		}			
			
		else 
			good=false;
					
		if (!good)
			this->operator()(who, i)=this->operator[](who)[i];				
	}
	else
	{
		good=false;
	}
	
	if (!good)
		cerr << line << " " << column << endl;

	return good;
}
	
bool Modele_epidemio_adhoc::Move(size_t who)	// move from one column to another with uniform probability on
					// (infected(i) \times susceptible(i+inc))
{
	size_t total=GetTotalCouples(who);
	size_t x=MyAlea::a.Uniform(total)+1;
	size_t i=0, j=0, sum=0;
	size_t inc=0;
		
	size_t line=0; 
	size_t column=0;

	// Determine where the exchange takes place
	while (x > sum)
	{
		size_t num=this->operator[](who)[i];
		
		line=i/m; 
		column=i%m;
		inc=0;
		
		switch(j%4)
		{
			case 0:
				if (column<m-1)
					inc=num*(N-this->operator[](ID_WILDTYPE)[i+1]-this->operator[](ID_MUTANT)[i+1]);
				break;
			
			case 1:
				if (line<n-1)
					inc=num*(N-this->operator[](ID_WILDTYPE)[i+m]-this->operator[](ID_MUTANT)[i+m]);
				break;
			
			case 2:
				if (column>0)
					inc=num*(N-this->operator[](ID_WILDTYPE)[i-1]-this->operator[](ID_MUTANT)[i-1]);
				break;
			
			case 3:
				if (line>0)
					inc=num*(N-this->operator[](ID_WILDTYPE)[i-m]-this->operator[](ID_MUTANT)[i-m]);
				break;
		}
		sum+=inc;
		j++;
		if ((j%4)==0)
		{
			i++;
		}		
	}
	
	j--;
	if ((j%4)==3)
		i--;
		
	long target=0;
	switch (j%4)
	{
		case 0:
			target=1;
			break;
		case 1:
			target=m;
			break;
		case 2:
			target=-1;
			break;
		case 3: 
			target=-m;
			break;
	}
		
	line=i/m; 
	column=i%m;
	
	// Makes the transaction
	bool good=true;
	
	if (this->operator[](who)[i]>0)
	{
		// Execute who's transaction (with check)
		this->operator()(who, i)=this->operator[](who)[i]-1;

		if (this->operator[](who)[i+target]<N)
			this->operator()(who, i+target)=this->operator[](who)[i+target]+1;
		else 
			good=false;
					
		if (!good)
			this->operator()(who, i)=this->operator[](who)[i];				
	}
	else
	{
		good=false;
	}
	
	if (!good)
		cerr << line << " " << column << endl;

	return good;
}

bool Modele_epidemio_adhoc::NewInfection(size_t who) 	// Convert a susceptible into an infected host.
{
	size_t total=GetNewInfections(who);
	size_t x=MyAlea::a.Uniform(total) + 1;
	size_t i=0, sum=0;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i]*(N-this->operator[](ID_WILDTYPE)[i]-this->operator[](ID_MUTANT)[i]);
		i++;
	}
	
	i--;
	
	if (this->operator[](who)[i] < N)
	{
		this->operator()(who, i)=this->operator[](who)[i]+1; 	// Schedule increase in population
		
		return true;
	}
	else
		return false;
}	

