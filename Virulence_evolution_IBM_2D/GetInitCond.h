/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/**********************************************************************************************
* GetInitCond.h                                                                               *
* Transforms a string into a vector of int                                                    *
**********************************************************************************************/

#ifndef __GET_INIT_COND_H__
	#define __GET_INIT_COND_H__
	
	
#include <iostream>
#include <string>
#include <sstream>
#include "Utilities/XML/SimpleXMLTree.h"

// Anything but numbers is ignored

vector<size_t> GetInitCond(const string & str, size_t increase_res=1000);

#endif

