/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Ad_hoc_Payoffs.h                                                           *
* Self-explaining : ad-hoc payoff class for the current program              *
*****************************************************************************/

/*

Here is a declaration summary : 

template<typename T, typename S=T> 
class paySum : public Payoff<T, S> // Sum of all elements (EVERY ELEMENT)

template<typename T, typename S=T>
class paySqSum : public Payoff<T, S> // Sum of squares (EVERY ELEMENT)

template<typename T, typename S=T> 
class paySum_oneline : public Payoff<T, S> // Sum of all elements (only one line)

template<typename T, typename S=T>
class paySqSum_oneline : public Payoff<T, S> // Sum of squares (only one line)

template<typename T, typename S=T>
class payLocalExchange : public Payoff<T, S> 	// Weird thing : calculates the probability of an exchange 
						// with a neighbour when all couples are equiprobable
						// Two types, which sum at most at Nmax

template<typename T, typename S=T>
class payScalarP : public Payoff<T, S> // scalar product of two lines

*/

#ifndef __AD_HOC_PAYOFFS_H__
	#define __AD_HOC_PAYOFFS_H__

#include "Payoff.h"
#include <iostream>

template<typename T, typename S=T> 
class paySum : public Payoff<T, S> // Sum of all elements (EVERY ELEMENT)
{
	public:
		paySum():Payoff<T, S>(0) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			Payoff<T, S>::result-=counters[counter] [desk];
			Payoff<T, S>::result+=newval;
		};
};
	
template<typename T, typename S=T>
class paySqSum : public Payoff<T, S> // Sum of squares (EVERY ELEMENT)
{
	public:
		paySqSum():Payoff<T, S>(0) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			S oldval=counters[counter] [desk];
			long inc=newval - counters[counter] [desk];
			Payoff<T, S>::result +=2*oldval*inc+inc*inc;
		};
};

template<typename T, typename S=T> 
class paySum_oneline : public Payoff<T, S> // Sum of all elements (only one line)
{
	protected:
		size_t line;
	public:
		paySum_oneline():Payoff<T, S>(0),  line(0) {};
		paySum_oneline(size_t l):Payoff<T, S>(0),  line(l) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			if (counter==line)
			{
				paySum_oneline<T, S>::result-=counters[counter] [desk];
				paySum_oneline<T, S>::result+=newval;
			}
		};
};

/********************* paySqSum_oneline *********************/
template<typename T, typename S=T>
class paySqSum_oneline : public Payoff<T, S> // Sum of squares (only one line)
{
	protected:
		size_t line;
	public:
		paySqSum_oneline():Payoff<T, S>(0), line(0) {};
		paySqSum_oneline(size_t l):Payoff<T, S>(0), line(l) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			if (counter == line)
			{
				S oldval=counters[counter] [desk];
				long inc=newval - counters[counter] [desk];
				Payoff<T, S>::result +=2*oldval*inc+inc*inc;
			}
		};
};

/********************* payLocalExchange *********************/
template<typename T, typename S=T>
class payLocalExchange : public Payoff<T, S> 	// Calculates the sum of product of two consecutive terms
{
	protected:
		size_t line;
	public:
		payLocalExchange():Payoff<T, S>(0), line(0) {};
		payLocalExchange(size_t l):Payoff<T, S>(0), line(l) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			if (line == counter)
			{
				long inc=newval - counters[counter] [desk];
				if (desk==0)
				{
					Payoff<T, S>::result +=inc*(counters[counter][desk+1]);
				}
				else
				{
					if (desk==counters[counter].size()-1)
					{
						Payoff<T, S>::result +=inc*(counters[counter][desk-1]);
					}
					else
					{
					Payoff<T, S>::result += inc*(counters[counter][desk+1]+counters[counter][desk-1]);
					}
				}
			}
		};
};


/********************* payScalarP *********************/
/* Beware : the lines must have the same length or you'll probably get an out-of-array exception */
template<typename T, typename S=T>
class payScalarP : public Payoff<T, S> // scalar product of two lines
{
	protected:
		size_t line1, line2;
	public:
		payScalarP():Payoff<T, S>(0), line1(0), line2(0) {};
		payScalarP(size_t l1, size_t l2):Payoff<T, S>(0), line1(l1), line2(l2) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			if (counter == line1)
			{
				long inc=newval - counters[counter] [desk];
				Payoff<T, S>::result +=counters[line2][desk]*inc;
			}
			
			if (counter == line2)
			{
				int inc=newval - counters[counter] [desk];
				Payoff<T, S>::result +=counters[line1][desk]*inc;
			}
		};
};

/********************* payLocalCross *********************/
template<typename T, typename S=T>
class payLocalCross : public Payoff<T, S> 	// Calculates the sum of product of two consecutive terms
						// between two lines
{
	protected:
		size_t line1, line2;
	public:
		payLocalCross():Payoff<T, S>(0), line1(0), line2(0) {};
		payLocalCross(size_t l1, size_t l2):Payoff<T, S>(0), line1(l1), line2(l2) {};
		
		void Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)
		{
			if (line1 == counter)
			{
				long inc=newval - counters[counter] [desk];
				if (desk==0)
				{
					Payoff<T, S>::result +=inc*(counters[line2][desk+1]);
				}
				else
				{
					if (desk==counters[counter].size()-1)
					{
						Payoff<T, S>::result +=inc*(counters[line2][desk-1]);
					}
					else
					{
					Payoff<T, S>::result += inc*(counters[line2][desk+1]+counters[line2][desk-1]);
					}
				}
			}
			if (line2 == counter)
			{
				long inc=newval - counters[counter] [desk];
				if (desk==0)
				{
					Payoff<T, S>::result +=inc*(counters[line1][desk+1]);
				}
				else
				{
					if (desk==counters[counter].size()-1)
					{
						Payoff<T, S>::result +=inc*(counters[line1][desk-1]);
					}
					else
					{
					Payoff<T, S>::result += inc*(counters[line1][desk+1]+counters[line1][desk-1]);
					}
				}
			}
		};
};

/********************* PayLocalExchange_Neumann *********************/
class payLocalExchange_Neumann : public Payoff<size_t, size_t> 
{
	protected:
		size_t line, n, m, pop;	// n : number of lines of the matrix. m : number of columns.
					// Don't screw up with the arguments, which are assumed to
					// contain enough space
	public:
		payLocalExchange_Neumann(size_t l, size_t N, size_t M, size_t Popsize):Payoff<size_t, size_t>(0), line(l), n(N), m(M), pop(Popsize) {};
		
		void Calc(const vector<size_t>* counters, size_t counter, size_t desk, const size_t & newval)
		{
			if (counter==line)
			{
				size_t i=desk / m;
				size_t j=desk % m;
							
				long inc=newval-counters[counter][desk];
			
				long sum=0;
				// Direct influence
				sum+=(j<m-1) ? pop-2*counters[counter][desk+1]:0;
				sum+=(i<n-1) ? pop-2*counters[counter][desk+m]:0;
				sum+=(j>0) ? pop-2*counters[counter][desk-1]:0;
				sum+=(i>0) ? pop-2*counters[counter][desk-m]:0;
				
				Payoff<size_t, size_t>::result += inc*sum;
			}
		};
};

/********************* PayLocalExchange_Neumann_with *********************/
// For two populations only
class payLocalExchange_Neumann_with : public Payoff<size_t, size_t> 
{
	protected:
		size_t line1, line2, n, m, pop;	// n : number of lines of the matrix. m : number of columns.
					// Don't screw up with the arguments, which are assumed to
					// contain enough space

	public:
		payLocalExchange_Neumann_with(size_t l1, size_t l2, size_t N, size_t M, size_t Popsize):Payoff<size_t, size_t>(0), line1(l1), line2(l2), n(N), m(M), pop(Popsize) {};
		
		void Calc(const vector<size_t>* counters, size_t counter, size_t desk, const size_t & newval)
		{
			if (counter==line1)
			{
				size_t i=desk / m;
				size_t j=desk % m;
							
				long inc=newval-counters[counter][desk];
			
				long sum=0;
				// Direct influence
				sum+=(j<m-1) ? pop-2*counters[counter][desk+1]-counters[line2][desk+1]:0;
				sum+=(i<n-1) ? pop-2*counters[counter][desk+m]-counters[line2][desk+m]:0;
				sum+=(j>0) ? pop-2*counters[counter][desk-1]-counters[line2][desk-1]:0;
				sum+=(i>0) ? pop-2*counters[counter][desk-m]-counters[line2][desk-m]:0;
				
				Payoff<size_t, size_t>::result += inc*sum;
			}
			
			if (counter==line2)
			{
				size_t i=desk / m;
				size_t j=desk % m;
							
				long inc=newval-counters[counter][desk];
			
				long sum=0;

				sum+=(j<m-1) ? -counters[line1][desk+1]:0;
				sum+=(i<n-1) ? -counters[line1][desk+m]:0;
				sum+=(j>0) ? -counters[line1][desk-1]:0;
				sum+=(i>0) ? -counters[line1][desk-m]:0;
				
				Payoff<size_t, size_t>::result += inc*sum;
			}			
		};
};

/********************* PayLocalExchange_Neumann_with_other *********************/
// For two populations only
class payLocalExchange_Neumann_with_other : public Payoff<size_t, size_t> 
{
	protected:
		size_t line1, line2, n, m;	// n : number of lines of the matrix. m : number of columns.
					// Don't screw up with the arguments, which are assumed to
					// contain enough space

	public:
		payLocalExchange_Neumann_with_other(size_t l1, size_t l2, size_t N, size_t M) : Payoff<size_t, size_t>(0), line1(l1), line2(l2), n(N), m(M){};
		
		void Calc(const vector<size_t>* counters, size_t counter, size_t desk, const size_t & newval)
		{
			if (counter==line1)
			{
				size_t i=desk / m;
				size_t j=desk % m;
							
				long inc=newval-counters[counter][desk];
			
				size_t sum=0;
				// Direct influence
				sum+=(j<m-1) ? counters[line2][desk+1]:0;
				sum+=(i<n-1) ? counters[line2][desk+m]:0;
				sum+=(j>0) ? counters[line2][desk-1]:0;
				sum+=(i>0) ? counters[line2][desk-m]:0;
				
				Payoff<size_t, size_t>::result += inc*sum;
			}
			
			if (counter==line2)
			{
				size_t i=desk / m;
				size_t j=desk % m;
							
				long inc=newval-counters[counter][desk];
			
				long sum=0;

				sum+=(j<m-1) ? counters[line1][desk+1]:0;
				sum+=(i<n-1) ? counters[line1][desk+m]:0;
				sum+=(j>0) ? counters[line1][desk-1]:0;
				sum+=(i>0) ? counters[line1][desk-m]:0;
				
				Payoff<size_t, size_t>::result += inc*sum;
			}			
		};
};

#endif

