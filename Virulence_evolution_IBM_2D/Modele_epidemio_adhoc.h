/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Modele_epidemio_adhoc.cpp                                                  *
* Self-explaining : this model is optimized for one purppse only             *
* This one codes a SI model at population equilibrium                        *
* note that the susceptible hosts are implicit (I+S=N)                       *
*****************************************************************************/

#ifndef __MODELE_EPIDEMIO_ADHOC_H__
	#define __MODELE_EPIDEMIO_ADHOC_H__
	
#include <iostream>

using namespace std;

#include "Local_counter_bank.h"
#include "Ad_hoc_payoffs.h"

#include "Utilities/Random/Alea.h"

#define ID_WILDTYPE 0
#define ID_MUTANT 1

#define ID_RES_POP 0
#define ID_RES_WPOP 0
#define ID_RES_MPOP 1
#define ID_RES_SQUARES 2
#define ID_RES_WSQUARES 2
#define ID_RES_MSQUARES 3
#define ID_RES_MOVE 4
#define ID_RES_WMOVE 4
#define ID_RES_MMOVE 5
#define ID_RES_EXCHANGE 6

#define ID_RES_SCALARP 6

class Modele_epidemio_adhoc : public Local_counter_bank<size_t, 2, size_t>
{
	protected:
		size_t N; 			// Total pop (infected+susceptibles)
		size_t m, n;			// n : number of lines
						// m : number of columns	
		paySum_oneline<size_t, size_t> 		wpop; 		// Population of wild type
		paySum_oneline<size_t, size_t> 		mpop; 		// Population of mutant
		paySqSum_oneline<size_t, size_t> 	wsquares; 	// Sum of squares, wild type
		paySqSum_oneline<size_t, size_t> 	msquares; 	// Sum of squares, mutant
		payLocalExchange_Neumann_with		wmove;
		payLocalExchange_Neumann_with		mmove;
		payScalarP<size_t, size_t>		scalarp;	// scalar product
		payLocalExchange_Neumann_with_other	exc;		// exchange wt<->m
		
	public:	
		Modele_epidemio_adhoc(const vector<size_t> & winit, const vector<size_t> & minit, size_t pop_local, size_t nlines, size_t ncolumns);
virtual 	~Modele_epidemio_adhoc() {};
		
inline		size_t GetPop(size_t who) { return GetPayValue(ID_RES_POP+who); };
				
		bool Kill(size_t who);
		bool Move(size_t who);
		bool Exchange();
		bool NewInfection(size_t who);
		bool Mutate(size_t who);
		
inline		size_t	GetTotalCouples(size_t who)
		{
			return GetPayValue(ID_RES_MOVE+who);
		};
		
inline		size_t 	GetExchange()
		{
			return GetPayValue(ID_RES_EXCHANGE);
		};
		
		inline		size_t 	GetNewInfections(size_t who)
		{		
			return N*GetPayValue(ID_RES_POP + who)-GetPayValue(ID_RES_SQUARES + who)-GetPayValue(ID_RES_SCALARP);
		};	
};	
#endif

