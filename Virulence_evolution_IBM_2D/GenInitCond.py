#!/usr/bin/env python3

#    This program does IBM simulations for an epidemiological model
#    
#    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
#    
#    This file is part of Virulence_evolution_IBM_2D.
#
#    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
#    
#    Contact : Quentin Griette <q.griette@gmail.com>



# Generate an initial condition 

from lxml import etree
import sys
import random

# Get arguments

r=10
N=10

try:
    n=int(sys.argv[1])
except IndexError:
    sys.stderr.write('Enter number of lines : ')
    n=int(sys.stdin.readline())
pass

try:
    m=int(sys.argv[2])
except IndexError:
    sys.stderr.write('Enter number of columns : ')
    m=int(sys.stdin.readline())
pass

try:
    N=int(sys.argv[3])
except IndexError:
    pass
pass

try:
    r=int(sys.argv[4])
except IndexError:
    pass
pass

# Initialize the elementtrees

root=etree.Element('root')
info=etree.Element('info')

m_elem=etree.Element('m')
n_elem=etree.Element('n')
N_elem=etree.Element('N')

wildtype=etree.Element('wildtype')
mutant=etree.Element('mutant')

m_elem.text=str(m)
n_elem.text=str(n)

info.append(n_elem)
info.append(m_elem)
info.append(N_elem)

root.append(info)

wildtype.text=""
mutant.text=""
# main loop

for i in range(0, n):
    for j in range(0, m):
        if ((i-int(n/2))**2+(j-int(m/2))**2 <= r**2):
            if random.randint(0,1)==0:
                wt=N
                mu=0
            else:
                wt=0
                mu=N
            pass
        else:
            wt=0
            mu=0
        pass
        
        wildtype.text+=str(wt)+' '
        mutant.text+=str(mu)+' '
    pass
pass

root.append(wildtype)
root.append(mutant)

print(str(etree.tostring(root, pretty_print=False, encoding='unicode')))

