/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Experiment.h                                                               *
* Experience de diffusion-mutation avec deux types                           *
*****************************************************************************/

#ifndef __EXPERIMENT_H__
	#define __EXPERIMENT_H__
	
#include <cstdlib>
#include <cmath>
#include <iostream>

#include "Modele_epidemio_adhoc.h"
#include "Utilities/Random/Gillespie.h"
#include "Utilities/Engine_event_XMLsample_base.h"
#include "GetInitCond.h"

#define EVENT_SAMPLE 			3
#define EVENT_GILLESPIE 		5
#define EVENT_START_SPEED_SAMPLE 	6
#define EVENT_SAMPLE_SPEED		7

class Experiment:public Engine_event_XMLsample_base
{
	protected:
		Modele_epidemio_adhoc * pheno; // La population
		Gillespie G; // Pour gerer l'aleatoire
		
		double lambda, wbeta, wd, wmu, mbeta, md, mmu, T;	// Parametres du modele
		size_t n, m, N;						// Parametres du modele
		string file_init_cond;					// Import the initial condition from this file	
	
		bool extra_verbose;					// Parametres du programme
		
		bool HandleEvent(size_t ev);				// Gestion des evenements
		
		bool SampleInit(SimpleXMLTree & temp, SimpleXMLTree & tp);	// Initialisation des echantillons
		bool Sample(SimpleXMLTree &);				// Echantillonnage
		bool SampleAtExit(SimpleXMLTree &);			// Echantillonnage de sortie
		
	public:	
		Experiment(double Lambda=1.0, double wBeta=2.0, double wD=0.5, double wm=0.01, double mBeta=4.0, double mD=1.0, double mm=0.01, size_t Nx=100, size_t My=100, size_t Nmax=100, double Tf=1);
virtual		~Experiment();		
};
	
#endif

