/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* SimpleXMLTree.h                                                            *
* Classe de gestion d'arbres avec export en XML                              *
* Le support XML se limite aux noeuds seuls                                  *
*****************************************************************************/

#ifndef __SIMPLE_XML_TREE_H__
	#define __SIMPLE_XML_TREE_H__
	
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>

using namespace std;

class SimpleXMLTree
{
	protected:
		string tag, text;
		
		vector<SimpleXMLTree *> children;
		
	public:
		SimpleXMLTree() {};
		SimpleXMLTree(const string & Tag, const string & Text="", size_t nchildren=10):tag(Tag), text(Text), children(nchildren, NULL) {};
		SimpleXMLTree(const SimpleXMLTree & cp):tag(cp.tag), text(cp.text), children(cp.children.size(), NULL) { for (size_t i=0; i<children.size(); i++) { if (cp.children[i] != NULL) { children[i]=new SimpleXMLTree(*cp.children[i]); } } }; 
		
virtual		~SimpleXMLTree() { for ( size_t i=0; i<children.size(); i++) { if (children[i]!=NULL) { delete children[i]; } } };

		// Accessors
inline		string 		GetTag() const { return tag; };
inline		string &	GetText() { return text; };
inline 		SimpleXMLTree *	GetChild(size_t i) { if (i<children.size()) { return children[i]; } else { return NULL; } };
		// Returns the size of the children vector
inline		size_t		GetSize() const { return children.size(); };

inline		void		MakeRoom(size_t for_n) { for (size_t i=0; i<children.size(); i++) { if ((children[i]==NULL)&& (for_n>0)) { for_n--; } } children.resize(children.size()+for_n); };

		void		CleanChildren(bool rec=false);

		// Appends an existing XML Tree as a child of the current tree.
		// The class then handles the memory : the best thing to do is to forget about the pointer.
		// Any modification of this pointer will modify the element in the tree
		void 		Append(SimpleXMLTree * child);
		
inline		bool		Remove(size_t i) { if (GetChild(i)!=NULL) { children[i]=NULL; return true; } else { return false; } };
inline		bool		Destroy(size_t i) { if (GetChild(i)!=NULL) { delete children[i]; children[i]=NULL; return true; } else { return false; } };

		size_t 		find(const string & Tag) const;
		vector<size_t>	findall(const string & Tag) const;
		
		bool 		NoChild() const { bool ret=true; for (size_t i=0; i<children.size(); i++) { ret &= (children[i]==NULL); } return ret; };
			
friend		ostream & operator<<(ostream & out, const SimpleXMLTree & tp) // Raw print
		{
			out << "<" << tp.tag << ">" << tp.text;
			for (size_t i=0; i<tp.children.size(); i++)
			{
				if (tp.children[i]!=NULL)
				{
					out << *((tp.children)[i]);
				}
			}
			
			out << "</" << tp.tag << ">";
			
			return out;
		};
		
		void XMLdeclare(ostream & out) const
		{
			out << "<?xml version=\"1.0\"?>" << endl;
		};
		
		void NicePrint(ostream & out, const string & indent="", const string & inc="  ", bool XMLinfo=false, size_t min_indent=15) const
		{
			if (XMLinfo)
			{
				XMLdeclare(out);
			}
			
			if (NoChild() && (text.size() <= min_indent))
			{
				if (text.size() > 0)
					out << indent << "<" << tag << ">" << text << "</" << tag << ">" << endl;
				else
					out << indent << "<" << tag << "/>" << endl;
			}
			else
			{			
				out << indent << "<" << tag << ">" << endl;
				if (text.size() > 0)
					out << indent << inc << text << endl;
			
				for (size_t i=0; i<children.size(); i++)
				{
					if (children[i]!=NULL)
					{
						children[i]->NicePrint(out, indent+inc, inc, false);
					}
				}
			
				out << indent << "</" << tag << ">" << endl;
			}
		};
		
		// Parse a file containing XML
		// Name version
		// Please note that this will effectively change the tag of the current tree
		
		bool Parse(const string & name)
		{
			ifstream xmlfile;
			
			xmlfile.open(name.c_str(), fstream::in);
			
			if (xmlfile.is_open())
			{
				return Parse(xmlfile, true);
			}
			else
			{
				xmlfile.open((name + ".xml").c_str(), fstream::in);
				
				if (xmlfile.is_open())
				{
					return Parse(xmlfile, true);
				}
				else
				{
					return false;
				}
			}
		};
		
		// stream version
		// Must already be open
		
		bool Parse(istream & xmlin, bool first_time=false)
		{
			string strTag="";
			char cur_c='\0';
			bool flag=true;
			bool good=true;
			bool record_name=false;
			bool end=false;
			bool bad_unless_tagend=false;	
			bool check_xmldecl=false;
			bool check_comment=false;
			bool main_switch=true;
			bool main_switch_next_time=false;
			
			while (flag && good)
			{
				if (bad_unless_tagend)
				{
					good=false;
				}
				
				if (main_switch_next_time)
				{
					main_switch_next_time=false;
					main_switch=true;
				}

				xmlin.get(cur_c);

				if (xmlin.eof())
				{
					good=false;
					main_switch=false;
				}
				
				if (check_xmldecl && !xmlin.eof())
				{
					// XML declaration should follow the regex <\?xml' 'version=([0-9][0-9]*(\.[0-9]+)?)\?>
					// However, until the assimilation of a regex package, I will only look for '>'
					main_switch=false;
					if (cur_c=='>')
					{
						main_switch_next_time=true;
						record_name=false;
						check_xmldecl=false;
					}
				}

				if (check_comment && !xmlin.eof())
				{
					// Comments should follow the regex <--[^>]*>
					if (main_switch)
					{
						main_switch=false;
						good &= (cur_c=='-');
					}
					else
					{
						if (cur_c=='>')
						{
							main_switch_next_time=true;
							check_comment=false;
							record_name=false;
						}
					}
				}

				if (main_switch)
				{
					switch (cur_c)
					{
						case '<':
						{
							if (!record_name)
							{
								record_name=true;
							}
							else
							{
								good=false;
							}
						}
						break;
						
						case '/':
						{
							if (record_name)
							{
								if (strTag.size()==0)
									end=true;
								else
									bad_unless_tagend=true;
							}
							else
							{
								text+=cur_c;
							}
						}
						break;
						
						case '>':
						{
							if (record_name)
							{
								if (bad_unless_tagend)
									good=true;
								if (end)
								{
									if (strTag==tag)
										flag=false;
									else
										good=false;
								}
								else
								{
									if (first_time)
									{
										first_time=false;
										tag=strTag;
										text="";
									}
									else
									{
										SimpleXMLTree * tempTree=NULL;
									
										tempTree=new SimpleXMLTree(strTag);
										if (!bad_unless_tagend)
											good&=tempTree->Parse(xmlin, false);
										this->Append(tempTree);

									}
								}
							}
							else
							{
								good=false;
							}

							bad_unless_tagend=false;
							record_name=false;
							end=false;
							strTag="";
						}
						break;
						
						default:
						{
							if (record_name)
							{	
								if (strTag.size()==0)
								{
									switch(cur_c)
									{
										case '?':
											if (first_time)
												check_xmldecl=true;
											else
												good=false;
											break;

										case '-':
											check_comment=true;
											break;

										default:
											good&=(cur_c<='z') | (cur_c>='a');
											strTag+=cur_c;
									}
								}
								else
								{
									good&=(cur_c <= 'z') | (cur_c>='a') | (cur_c>='0') | (cur_c<='9') | (cur_c=='_');
									strTag += cur_c;
								}
							}
							else
								text+=cur_c;
						}
					}
				}
			}
			
			return good;
		};
};
	
#endif

