/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* SimpleXMLTree.cpp                                                          *
* Classe de gestion d'arbres avec export en XML                              *
* Le support XML se limite aux noeuds seuls                                  *
*****************************************************************************/

#include "SimpleXMLTree.h"


void 	SimpleXMLTree::Append(SimpleXMLTree * child)
{
	size_t i=0;
	
	if (children.size()==0)
	{
		children.resize(1);
		children[0]=NULL;
	}

	while ((children[i]!=NULL) && (i<children.size()))
	{
		i++;
	}
	
	if (i<children.size())
	{
		children[i]=child;
	}
	else
	{
		children.push_back(child);
	}
}

void 	SimpleXMLTree::CleanChildren(bool rec)
{
	size_t i=children.size();
	
	while ((i>0) && (children[i-1]==NULL))
	{
		i--;
	}
	
	size_t j=0;
	
	while (j<i) 
	{
		 if (children[j]==NULL)
		 {
		 	children[j]=children[i-1];
		 	children[i-1]=NULL;
		 	i--;
		 }
		 else
		 {
		 	if (rec)
		 	{
		 		children[j]->CleanChildren(rec);
		 	}
		 }
		 j++;
	}
	children.resize(i);
}

size_t SimpleXMLTree::find(const string & Tag) const
{
	size_t i=0;
	bool flag=true;
	
	while ((i<children.size()) && flag)
	{
		if (children[i]!=NULL)
		{
			if (children[i]->GetTag() == Tag)
			{
				flag=false;
			}
			else
			{
				i++;
			}
		}
		else
		{
			i++;
		}
	}
	
	return i;
}

vector<size_t>	SimpleXMLTree::findall(const string & Tag) const
{
	size_t j=0;
	vector<size_t> ret(children.size(), 0);
	
	for (size_t i=0; i<children.size(); i++)
	{
		if (children[i]!=NULL)
		{
			if (children[i]->GetTag() == Tag)
			{
				ret[j]=i;
				j++;
			}
		}
	}
	
	ret.resize(j);
	
	return ret;
}

