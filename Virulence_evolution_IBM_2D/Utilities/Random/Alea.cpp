/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Alea.cpp                                                                   *
* Gestion de l'aleatoire : encapsulation de gsl rand                         *
*****************************************************************************/

#include "Alea.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Alea::Alea(unsigned long int seed)
{
	// On alloue de la memoire au generateur pour un generateur de Mersenne
	gen=gsl_rng_alloc(gsl_rng_mt19937);
	// Initialisation
	gsl_rng_set(gen, seed);
}

/*****************************************************************************
* Desctucteur                                                                *
*****************************************************************************/

Alea::~Alea()
{
	// Liberation de la memoire a la destruction
	gsl_rng_free(gen);
}

/*****************************************************************************
* V.a. uniforme sur [0, 1)                                                   *
*****************************************************************************/

double Alea::X() const
{
	return gsl_rng_uniform(gen);
}

/*****************************************************************************
* V.a. uniforme sur [[0, n-1]] (loi discrete)                                *
*****************************************************************************/

size_t Alea::Uniform(size_t n) const
{
	return ((size_t) (n*X()));
}

/*****************************************************************************
* V.a. de loi exponentielle a taux lambda                                    *
*****************************************************************************/

double Alea::Exp(double lambda) const
{
	if (lambda > 0)
	{
		// 1-X() sert a eviter log(0)
		double x=-(1/lambda)*log(1-X());
		return x;
	}
	else
	{
		return 0;
	}
}

/*****************************************************************************
* V.a. de loi gaussienne de variance sigma                                   *
*****************************************************************************/

double Alea::Gauss(double sigma) const
{
    if (sigma>0)
    {
        return gsl_ran_gaussian(gen, sigma);
    }
    else
    {
        return 0;
    } 
}
 
