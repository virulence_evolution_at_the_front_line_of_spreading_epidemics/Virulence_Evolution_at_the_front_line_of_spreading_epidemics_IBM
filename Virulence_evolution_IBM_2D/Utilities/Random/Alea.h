/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Alea.h                                                                     *
* Gestion de l'aleatoire : encapsulation de gsl rand                         *
*****************************************************************************/

#ifndef __ALEA_H__
	#define __ALEA_H__
	
#include <ctime>
#include <cstdlib>
#include <cmath>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

class Alea
{
	private:
		gsl_rng * gen;
		
	public:
		Alea(unsigned long int seed);
		~Alea();
		
		double X() const;  			// Loi uniforme sur [0,1)
		size_t Uniform(size_t n) const; 	// Loi uniforme sur [[0, n-1]]
		double Exp(double lambda) const; 	// Loi exponentielle de taux lambda
		double Gauss(double sigma=1) const;
};

namespace MyAlea
{
// generateur aleatoire global : initilise au lancement du programme
static	Alea a(time(NULL));
}
		
#endif

