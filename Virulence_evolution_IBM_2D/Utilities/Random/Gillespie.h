/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Gillespie.h                                                                *
* Implementation de la methode de Gillespie                                  *
*****************************************************************************/

#ifndef __GILLESPIE_H__
	#define __GILLESPIE_H__
	
#include <vector>

using namespace std;

#include "Alea.h"

class Gillespie
{
	private:
		vector<double> 	taux; // L'evenement i intervient au taux i
		double 		sum;  // Somme des evenements (pour gagner du temps)
		
	public:
		Gillespie();
		Gillespie(size_t N); 			// Initialise tous les taux a 0
		Gillespie(const vector<double> &);	// Initialise les taux grace a l'argument
		Gillespie(const Gillespie &);		// Copie le contenu
		
		// Acces a un element
		double 	operator[](size_t i) const { return (i<taux.size()) ? taux[i] : 0; };
		double 	GetSum() const { return sum; }; // Acces a la somme
		size_t	GetSize() const { return taux.size(); }; // Acces au nombre d'elements
		
		bool	Set(size_t i, double t); 	// Met l'evenement i au taux t. 
							// retourne false si i > size ou t<0
		bool 	Update(vector<double> &); 	// met a jour l'ensemble des taux
		void 	Update();			// Met a jour la somme
		
		size_t	GetNextEvent() const; 		// retourne l'evenement suivant
		double 	GetNextTime() const;		// retourne la date de l'evenement suivant
};
		
#endif

