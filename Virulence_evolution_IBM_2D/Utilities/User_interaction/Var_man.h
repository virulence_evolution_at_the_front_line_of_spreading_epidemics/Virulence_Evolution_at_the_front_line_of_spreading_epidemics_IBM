/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Classe generique pour la gestion de variables typees                       *
* La classe comprend une analyse de chaine (voir fonction handle)            *
* Une chaine est associee a un type seulement et une seule valeur.           *
*****************************************************************************/

#ifndef __VAR_MAN_H__
    #define __VAR_MAN_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include "SimpleXMLTree.h"

using namespace std;

class Var_man
{
	private:
            map<string, int>                  	entiers;
            map<string, size_t>			        type_taille;

            map<string, float>              	flottants;
            map<string, double>               	flottants_doubles;

            map<string, bool>                 	booleens;

            map<string, string>               	chaines;
            
            string				seq_affect; 	// sequence de caracteres utilisee
            							// pour affecter une valeur a un nom

   	public:
    					Var_man(string seq_aff="=");

virtual					~Var_man() {};

		bool           	GetIntValue(const string &s, int & ret_value) const; // s est le nom du parametre dont on veut recuperer la valeur
		bool			GetSize_tValue(const string &, size_t & ret_value) const; // bool contient true si la valeur a ete trouvee
		bool           	GetFloatValue(const string &s, float & ret_value) const;
		bool           	GetDoubleValue(const string &s, double & ret_value) const;
		bool           	GetBoolValue(const string &s, bool & ret_value) const;
		bool           	GetStringValue(const string &name, string & ret_value) const;

		bool           	AddInt(const string &s, const int &value);                    // s est le nom du parametre dont on veut recuperer la valeur
		bool			AddSize_t(const string &, const size_t & value);	// bool contient true si la valeur a ete trouvee
		bool           	AddFloat(const string &s, const float &value);
		bool           	AddDouble(const string &s, const double &value);
		bool           	AddBool(const string &s, const bool &value);
		bool           	AddString(const string &name, const string &value);
		bool			AddString(const string &name, const char * value);

// Retourne true si lr parametre a ete declare
		bool			IsInt(const string &name) const;
		bool			IsSize_t(const string & name) const;
		bool           	IsFloat(const string &name) const;
		bool           	IsDouble(const string &name) const;
		bool           	IsBool(const string &name) const;
		bool           	IsString(const string &name) const;

// Analyse de chaine
virtual		bool       	handle(const string &argt);

// Affichage des parmetres declares
		string        	Display_all(bool value=true) const;

// Uniformisation des notations
		bool           	Get(const string &s, int & ret_value) const;
		bool			Get(const string &s, size_t & ret_value) const;
		bool          	Get(const string &s, float & ret_value) const;
		bool           	Get(const string &s, double & ret_value) const;
		bool           	Get(const string &s, bool & ret_value) const;
		bool           	Get(const string &s, string & ret_value) const;

		bool           	Add(const string &s, const int &value);
		bool 			Add(const string &s, const size_t &value);
		bool           	Add(const string &s, const float &value);
		bool           	Add(const string &s, const double &value);
		bool           	Add(const string &s, const bool &value);
		bool           	Add(const string &s, const string &value);
		bool			Add(const string &s, const char * value);

		bool           	SetVar(const string &s, const int &value);
		bool 			SetVar(const string &s, const size_t &value);
		bool           	SetVar(const string &s, const float &value);
		bool           	SetVar(const string &s, const double &value);
		bool           	SetVar(const string &s, const bool &value);
		bool           	SetVar(const string &s, const string &value);

		bool           	Is(const string &) const;
		bool			IsAvailable(const string &) const;

// Afficher une variable si elle est presente
		bool           	Display(const string &name, string &ret) const; 
            
		string			GetSeqAffect() const;
		void			SetSeqAffect(const string &new_seq);
		
		bool          	GetIntXML(SimpleXMLTree &, const string & name) const; // s est le nom du parametre dont on veut recuperer la valeur
		bool			GetSize_tXML(SimpleXMLTree &, const string & name) const; // bool contient true si la valeur a ete trouvee
		bool           	GetFloatXML(SimpleXMLTree &, const string & name) const;
		bool           	GetDoubleXML(SimpleXMLTree &, const string & name) const;
		bool           	GetBoolXML(SimpleXMLTree &, const string & name) const;
		bool           	GetStringXML(SimpleXMLTree &, const string & name) const;

		bool			GetVarXML(SimpleXMLTree &, const string & name) const;
		void			GetVarXML(SimpleXMLTree &) const;
};

#endif

