/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Classe generique pour la gestion de commandes passees en ligne.            *
* Les parametres les plus courants d'arguments sont pris en compte.          *
* La classe herite de Var_man.                                               *
* Attention a ne pas donner le meme nom a des varibles et des commandes      *
*****************************************************************************/

#include "Cmd_man.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Cmd_man::Cmd_man(string seq_aff, string pre)
	:Var_man(seq_aff), prefix(pre)
{
}

/*****************************************************************************
* Accesseurs, mutateurs                                                      *
*****************************************************************************/

void Cmd_man::Set(const string & name, const string & help, bool val)
{
	Commandes[name]=val;
	Description[name]=help;
}

void Cmd_man::SetVal(const string & name, bool val)
{
	Commandes[name]=val;
}

void Cmd_man::SetHelp(const string & name, const string & help)
{
	Description[name]=help;
}

bool Cmd_man::GetCmd(const string & name, bool & val) const
{
	if (Commandes.count(name)!=0)
	{
		map<string, bool>::const_iterator it;
		
		it=Commandes.find(name);
		val=it->second;
		
		return true;
	}
	return false;
}

/*****************************************************************************
* void Display_help()                                                        *
* Affiche la description donnee a la creation d'une commande en correlation  *
* avec la commande, ainsi que la liste des variables                         *
*****************************************************************************/

void Cmd_man::Display_help(ostream & out, bool values) const
{
	out << "    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. \n \
    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. \n \
    You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>. \n " << endl;
    out << "Commandes : " << endl;
    
    if (!Commandes.empty())
    {
        map<string, string>::const_iterator it;

        for (it = Description.begin() ; it != Description.end() ; it ++)
        {
            out << " * " << it->first;
            if (it->second.size()!=0)
            	out << " : " << it->second;
            out << endl;
        }
    }
    else
    {
    	out << "Aucune commande" << endl;
    }
    out << endl;
    
    out << "Variables : " << endl;
    out << Display_all(values) << endl;
    out << endl;    
}
   
/*****************************************************************************
* void handle()                                                              *
* Traite une ligne de commandes separees par des espaces                     *
*****************************************************************************/

bool Cmd_man::handle(const string &argt)
{	
	size_t wpos;

    	wpos = argt.find_first_of(' ');

    	if (wpos != string::npos)
    	{
        	string temp1 = argt.substr(0, wpos);
        	string temp2 = argt.substr(wpos + 1);

        	return handle(temp1) & handle(temp2);
    	}
    	
    	if (Commandes.count(argt)!=0)
    	{
    		Commandes[argt]=true;
    		
    		return true;
    	}
    	else
    	{
    	    	return Var_man::handle(argt);
    	}
}

/*****************************************************************************
* bool GetCmdXML(SimpleXMLTree &, string, bool)                              *
* Appends an XML version of the command to a tree                            *
*****************************************************************************/

bool Cmd_man::GetCmdXML(SimpleXMLTree & ret, string name) const
{
	bool val;
	
	if (GetCmd(name, val))
	{
		if (val)
		{
			// use the name without prefix. This may change in the future
			ret.Append(new SimpleXMLTree(name.substr(prefix.size()), "", 0));
		}
		
		return true;
	}
	
	return false;
}

/*****************************************************************************
* bool GetCmdXML(SimpleXMLTree &, bool)                                      *
* Appends an XML version of the commands to an existing tree                 *
* Returns true if at least one modification has been made to the tree        *
*****************************************************************************/

bool Cmd_man::GetCmdXML(SimpleXMLTree & ret) const
{
	bool flag=false;
	ret.MakeRoom(Commandes.size()+1);
	
	ret.Append(new SimpleXMLTree("prefix", prefix, 0));
	
	map<string, bool>::const_iterator it;
	
	for (it=Commandes.begin(); it != Commandes.end(); it++)
	{
		GetCmdXML(ret, it->first);
		if (it->second)
			flag=true;
	}
	
	return flag;
}

