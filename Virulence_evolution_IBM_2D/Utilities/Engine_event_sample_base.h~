/*****************************************************************************
* Engine_event_sample_base.h                                                 *
* Classe de base pour un moteur de gestion des evenements                    *
* Preconfiguration d'enregistrements reguliers                               *
*****************************************************************************/

#ifndef __ENGINE_EVENT_SAMPLE_BASE_H__
	#define __ENGINE_EVENT_SAMPLE_BASE_H__

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Engine_event_base.h"
	
#define EVENT_SAMPLE 	3

class Engine_event_sample_base:public Engine_event_base
{
	private:		
		double	sample_every;
		bool 	sample_at_exit;
				
	protected:
		// Fonction a surcharger pour gerer les evenements
inline virtual	bool HandleEvent_0(size_t event);
virtual		bool HandleEvent(size_t event);
		// Fonctions a surcharger pour les enregistrements
virtual		bool SampleInit();
virtual		bool Sample();
virtual		bool SampleAtExit();

	public:	
		Engine_event_sample_base(double se=0, bool sae=false, string ef=string(""), bool fc=false, bool h=false, bool v=false);
		
virtual		~Engine_event_sample_base() {};

};

/*****************************************************************************
* Gestion des evenements et echantillonnage                                  *
*****************************************************************************/
	
inline bool Engine_event_sample_base::HandleEvent_0(size_t event)
{
	bool ret=true;
	
	switch (event)
	{
		case EVENT_INIT:
			Get("sample_every", sample_every);
			GetCmd("--sample_at_exit", sample_at_exit);
			
			ret &= HandleEvent(event);
			
			ret &= SampleInit();
			
			if (sample_every != 0)
			{
				evman.Push_Event(sample_every, EVENT_SAMPLE);
			}
			
			break;
		
		case EVENT_SAMPLE:
			ret &= Sample();
			
			if (Verbose() && (!ret))
			{
				cerr << evman.GetTime() << "Something went wrong while sampling" << endl;
			}
			
			if (sample_every != 0)
			{
				evman.Push_Event(evman.GetTime() + sample_every, EVENT_SAMPLE);
			}
			
			break;
			
		case EVENT_AT_EXIT:
			if (sample_at_exit)
				ret &= SampleAtExit();
			
			if (Verbose() && (!ret))
				cerr << "Erreur a l'echantillonnage final" << endl;
				
			ret &= HandleEvent(event);
			
			break;
			
		default:
			HandleEvent(event);
	}
	
	return ret;
}

#endif

