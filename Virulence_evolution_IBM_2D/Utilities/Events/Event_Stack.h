/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Event_Stack.h                                                              *
* Pile d'evenements                                                          *
*****************************************************************************/

#ifndef __EVENT_STACK_H__
	#define __EVENT_STACK_H__
	
#include <cmath>

#include "Event.h"

template<typename corpus_type>
class Event_Stack
{
	protected:
		Event<corpus_type> * e;
	
	public:
		Event_Stack() ;
		~Event_Stack();
		
		// Ajouter un evenement (dans l'ordre chronologique)
		void 			Push_Event(double t, corpus_type c);
		
		double 			              GetTime() const { return (e==NULL)? +nan("") : e->time; };
		bool			              GetCorpus(corpus_type &) const;
		const corpus_type * 	      GetCorpus() const;
		const Event<corpus_type> *    GetNext() const { return e->next; };
		
		bool			Is() const { return !(e==NULL); };
		bool 			NextEvent(); // Detruit l'evenement courant et charge le suivant.
};

template<typename corpus_type>
Event_Stack<corpus_type>::Event_Stack()
	:e(NULL)
{
}

template<typename corpus_type>
Event_Stack<corpus_type>::~Event_Stack()
{
	if (e != NULL)
		delete e;
}
		
template<typename corpus_type>
void Event_Stack<corpus_type>::Push_Event(double t, corpus_type c)
{
	Event<corpus_type> * ev;
	
	ev=new Event<corpus_type>(t, c);
	
	if (e==NULL)
	{
		e=ev;
	}
	else
	{
		// Insertion dans l'ordre chronologique
		// L'insertion en tete doit etre decidee a ce niveau
		if (t < e->GetTime())
		{
			ev->next=e;
			e=ev;
		}
		else
		{
			// Insertion
			e->Insert(ev);
		}
	}
}

template<typename corpus_type>
bool Event_Stack<corpus_type>::GetCorpus(corpus_type & c) const
{
	if (e!=NULL)
	{
		c=corpus_type(e->corpus);
		return true;
	}
	
	return false;
}

template<typename corpus_type>
const corpus_type * Event_Stack<corpus_type>::GetCorpus() const
{
	if (e==NULL)
		return NULL;
	else
	{
		return &e->corpus;
	}
}
	
template<typename corpus_type>
bool Event_Stack<corpus_type>::NextEvent()
{
	if (e==NULL)
		return false;
	else
	{
		Event<corpus_type> * temp; 
		
		temp=e->next;
		
		// D'abord ejecter, puis detruire
		e->Eject();
		delete e; 
		
		e=temp;
		
		return true;
	}
}

#endif

