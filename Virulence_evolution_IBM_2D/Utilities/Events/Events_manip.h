/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Events_manip.h                                                             *
* Manipulation (et stockage) d'evenements                                    *
*****************************************************************************/

#ifndef __EVENTS_MANIP_H__
	#define __EVENTS_MANIP_H__

#include <istream>
#include <ostream>

using namespace std;

#include "Event_Stack.h"	
	
template<typename corpus_type>
class Events_manip:public Event_Stack<corpus_type>
{
	public:
	
/* 	Inherited from Events_Stack : 		
		void 			Push_Event(double t, corpus_type c);
		
		double 			GetTime() const { return (e==NULL)? +nan(NULL):e->time; };
		bool			GetCorpus(corpus_type &) const;
		const corpus_type * 	GetCorpus() const;
		
		bool			Is() const { return !(e==NULL); };
		bool 			NextEvent(); 
*/
		
friend 	ostream& operator << (ostream & out, const Events_manip<corpus_type> & em) 
{
	const Event<corpus_type> * e=em.e;
	
	while (e!=NULL)
	{	
		out <<  e->GetTime() << " " << e->GetCorpus() << endl;
		
		
		e=e->GetNext();
	}
	
	return out;
}
		
friend 	istream& operator >> (istream & in, Events_manip<corpus_type> & em)
{
	while (!in.eof())
	{
		corpus_type c;
		double t;
		
		in >> t;
		
		if (in.good())
		{
			in >> c;
			
			if (in.good())
			{
				em.Push_Event(t, c);
			}
			else
			{
				cerr << "Erreur a la lecture de l'evenement" << c << endl;
				return in;
			}
		}
		else
		{
			if (!in.eof())
			{
				cerr << "Erreur a la lecture du temps : " << t << endl;
				return in;
			}
		}
	}
	
	return in;
}

};	
	
#endif
	
