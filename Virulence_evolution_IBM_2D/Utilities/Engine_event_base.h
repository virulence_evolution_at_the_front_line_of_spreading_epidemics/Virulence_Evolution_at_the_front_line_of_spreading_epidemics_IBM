/*****************************************************************************
* Engine_event_base.h                                                        *
* Engine with integrated events handler                                      *
*****************************************************************************/

#ifndef __ENGINE_EVENT_BASE_H__
	#define __ENGINE_EVENT_BASE_H__

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Engine_base.h"
#include "Events/Events_manip.h"	
	
#define EVENT_EXIT 	0
#define EVENT_INIT 	1
#define EVENT_AT_EXIT 	2

class Engine_event_base:public Engine_base
{
	private:		
		// file to read events from
		string 	events_file;
		// Read from keyboard
		bool events_from_cin;
		
virtual		int Internal_loop_0(); // Main loop
		
	protected:
		// Stack of events
		Events_manip<size_t> evman;
		// Events handler
virtual		bool HandleEvent_0(size_t event)=0;
		
	public:	
		Engine_event_base(string ef=string(""), bool fc=false, bool h=false, bool v=false);
		
virtual		~Engine_event_base() {};

};	
	
#endif

