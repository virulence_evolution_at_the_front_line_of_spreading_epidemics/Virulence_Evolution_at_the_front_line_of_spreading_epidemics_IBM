/*****************************************************************************
* Engine_base.cpp                                                            *
* Classe de base pour un moteur de programme                                 *
*****************************************************************************/

#include "Engine_base.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Engine_base::Engine_base(bool h, bool v)
	:help(h), verbose(v)
{
	// Configuration du mode "aide"
	Set("--help", "Affiche l'aide (+ --verbose pour voir les valeurs des parametres)",  help);
	// verbose sera utile dans les classes derivees
	Set("--verbose", "Mode bavard", verbose);
}

/*****************************************************************************
* Recuperation des arguments passes en ligne de commande                     *
*****************************************************************************/

bool Engine_base::Start(size_t argc, const char * argv [])
{
	bool ret=true;
	
	if (argc > 1)
	{
		for (size_t i= 1; i < argc; i++)
		{
			bool flag=false;
			
			flag|=handle(argv[i]);
			
			if (!flag)
			{
				cerr << "Argument incorrect : " << argv[i] << endl;
				cerr << "Pour obtenir de l'aide : " << argv[0] << " --help" << endl;
			}
			
			ret &= flag;
		}
		
		return ret;
	}
	else
	{
		return true;
	}
}


/*****************************************************************************
* Boucle principale                                                          *
*****************************************************************************/

int Engine_base::Loop()
{
	// Recuperation des informations
	GetCmd("--help", help);
	GetCmd("--verbose", verbose);
	
	// Si l'option --help est active, on affiche l'aide et on quitte.
	if (help)
	{
		if (verbose) // Si verbose, on affiche les valeurs des parametres
			Display_help(cout, true);
		else
			Display_help(cout, false);
			
		return 0;
	}
	else // Sinon on passe a la boucle interne
	{
		return Internal_loop_0();
	}
}

