/*****************************************************************************
* Engine_event_XMLsample_base.h                                              *
* Classe de base pour un moteur de gestion des evenements                    *
* Preconfiguration d'enregistrements reguliers                               *
* Gestion basique de XML                                                     *
*****************************************************************************/

#ifndef __ENGINE_EVENT_XML_SAMPLE_BASE_H__
	#define __ENGINE_EVENT_XML_SAMPLE_BASE_H__

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Engine_event_base.h"
#include "XML/SimpleXMLTree.h"
	
#define EVENT_SAMPLE 			3

#define OUTPUT_SAMPLE_AS_XML ("xml")
#define OUTPUT_SAMPLE_AS_CSV ("csv")

class Engine_event_XMLsample_base : public Engine_event_base
{
	protected:		
		double	sample_every;
		string 	sample_out;
		
		bool 	sample;
		bool 	sample_at_exit;	
		bool 	no_declare_XML;
		bool	evil_print;
		
		string 	kind_of_output, csv_field_separator, csv_record_separator;
					
		SimpleXMLTree 	sample_template;
		SimpleXMLTree *	sample_toprint;

		string indent, inc;
		
		void Print(bool force_xml=false, bool first_time=false, bool close=false);
		bool Format_template();
		size_t  nsamples;
		
		bool    sample_after;   // If this is true, the engine will first check for other events before sampling. If there exists samples at the same time, it will wait until they are complete before sampling

		// Fonction a surcharger pour gerer les evenements
virtual		bool HandleEvent_0(size_t event);
virtual		bool HandleEvent(size_t event)=0;
		// Fonctions a surcharger pour les enregistrements
virtual		bool SampleInit(SimpleXMLTree &, SimpleXMLTree &)=0;
virtual		bool Sample(SimpleXMLTree &)=0;
virtual		bool SampleAtExit(SimpleXMLTree &)=0;

	public:	
		Engine_event_XMLsample_base(double se=0, const string & so="", bool sae=false, bool s=false, bool nxd=false, const string & ef=string(""), bool fc=false, bool h=false, bool v=false, string kou="xml", string csv_field_sep=";", string csv_rec_sep="\n", bool sa=false);
		
virtual		~Engine_event_XMLsample_base() { if (sample_toprint != NULL) { delete sample_toprint; } };

};

/*****************************************************************************
* Gestion des evenements et echantillonnage                                  *
*****************************************************************************/
	
inline bool Engine_event_XMLsample_base::HandleEvent_0(size_t event)
{
	bool ret=true;

	switch (event)
	{
		case EVENT_INIT:
			Get("sample_every", sample_every);
			Get("sample_out", sample_out);
			Get("kind_of_output", kind_of_output);
			Get("csv_field_separator", csv_field_separator);
			Get("csv_record_separator", csv_record_separator);
			
			GetCmd("--sample", sample);
			GetCmd("--sample_at_exit", sample_at_exit);
			GetCmd("--no_declare_XML", no_declare_XML);
			GetCmd("--evil_print", evil_print);
			
			ret &= HandleEvent(event);
			
			// Initializes the XML sheet if sampling is activated
			if ((sample) || (sample_at_exit))
			{
				sample_toprint=new SimpleXMLTree("info");
				
				ret &= SampleInit(sample_template, *sample_toprint);
			
				sample_template.CleanChildren();
				
				// Declares XML, opens root node and prints sheet information AS XML 
				Print(true, true);
				
				// Free the memory of the XML tree
				delete sample_toprint;
				sample_toprint=NULL;
			}
			
			if ((sample_every != 0) && (sample))
			{
				evman.Push_Event(sample_every, EVENT_SAMPLE);
			}
			
			break;
		
		case EVENT_SAMPLE:
		{
		    bool sample_at_this_time=false;
		    
		    if (sample_after) // Mechanism to postpone sampling after simultaneous actions if required 
		    {
		        if (evman.GetNext()->GetTime()<=evman.GetTime())
		        {
		            evman.Push_Event(evman.GetTime(), EVENT_SAMPLE);
		        }
		        else
		            sample_at_this_time=true;
		    }
		    else
		    {
		    	sample_at_this_time=true;
		    }
		    
		    if (sample_at_this_time)    // Proper sampling
		    {
			    sample_toprint=new SimpleXMLTree(sample_template);
			
			    ret &= Sample(*sample_toprint);
			
			    if (ret)
			    {
				    Print(false);
			    }
			    else
			    {
				    if (Verbose())
				    {
					    cerr << evman.GetTime() << "Something went wrong while sampling" << endl;
				    }
			    }
			
			    delete sample_toprint; 
			    sample_toprint=NULL;
			
			    // Schedule next sample
			    if ((sample_every != 0) && (sample))
			    {
				    evman.Push_Event((++nsamples +1)*sample_every, EVENT_SAMPLE);
			    }
			}
		}
			
		break;
			
		case EVENT_AT_EXIT:
			if (sample_at_exit)
			{
				sample_toprint=new SimpleXMLTree(sample_template);
			
				ret &= SampleAtExit(*sample_toprint);
			}
			else
			{
				sample_toprint=new SimpleXMLTree("end");
			}
			
			if ((sample)|| (sample_at_exit))
				Print(false, false, true);
				
			delete sample_toprint;
			sample_toprint=NULL;

			if (Verbose() && (!ret))
				cerr << "Something went wrong with the last sample" << endl;
				
			ret &= HandleEvent(event);
			
			break;
			
		default:
			HandleEvent(event);
	}
	
	return ret;
}

	
#endif

