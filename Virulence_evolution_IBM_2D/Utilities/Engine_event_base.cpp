/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_2D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_2D.

    Virulence_evolution_IBM_2D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_2D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_2D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_event_base.cpp                                                      *
* Classe de base pour un moteur de gestion des evenements                    *
*****************************************************************************/

#include "Engine_event_base.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Engine_event_base::Engine_event_base(string ef, bool fc, bool h, bool v)
	:Engine_base(h, v), events_file(ef), events_from_cin(fc)
{
	// events_file contient le nom du fichier depuis lequel lire les evenements
	Add("events_file", events_file);
	// Configuration des commandes
	Set("--events_from_cin", "Read events from keyboard", events_from_cin);
	SetHelp("events_file", "File containing a list of events");
}

/*****************************************************************************
* Boucle principale                                                          *
*****************************************************************************/

int Engine_event_base::Internal_loop_0()
{
	// Recuperation des informations
	Get("events_file", events_file);
	GetCmd("--events_from_cin", events_from_cin);
	
	// Recuperation des evenements depuis le clavier si besoin
	if (events_from_cin)
	{
		cin >> evman;
	}
	
	// Recuperation des evenements depuis le fichier
	if (events_file.size() != 0)
	{
		ifstream fstr;
		
		fstr.open(events_file.c_str(), fstream::in);
		
		if ((fstr.is_open()) && (fstr.good()))
		{
			fstr >> evman;
		}
		
		fstr.close();
	}
	
	// Gestion des evenements
	bool flag=true;
	bool ret=true;
	
	// On lance un evenement d'initialisation
	ret &= HandleEvent_0(EVENT_INIT);
	
	// La boucle tourne jusqu'a ce que la pile soit vide (erreur) ou jusqu'a rencontrer EVENT_EXIT
	while (flag)
	{
		if (evman.Is())
		{
			size_t ev; // Evenement courant
			
			if (evman.GetCorpus(ev))
			{
				// On quitte si il s'agit de l'evenement final
				if (ev==EVENT_EXIT)
					flag=false;
				else // Gestion de l'evenement l'evenement
					ret &= HandleEvent_0(ev);
			}
			else // Si l'evenement n'est pas reconnu on signale l'erreur
			{
				cerr << evman.GetTime() << " : Unknown event" << endl;
			}
			
			// On passe a l'evenement suivant
			if (flag)
				evman.NextEvent();
		}
		else // Si la pile est vide on signale une erreur
		{
			cerr << "Unexpected exit" << endl;
			flag=false;
			ret=false;
		}
	}
	
	// Dernieres taches a effectuer
	ret &= HandleEvent_0(EVENT_AT_EXIT);
	
	if (!ret)
		cerr << "Errors have been detected" << endl;
		
	return 0;
}

