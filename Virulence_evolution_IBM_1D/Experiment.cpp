/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

#include "Experiment.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Experiment::Experiment(double Lambda, double wBeta, double wD, double wm, double mBeta, double mD, double mm, size_t Nx, size_t Nmax, double Tf)
	:Engine_event_XMLsample_base(0, "", false, false, false, "", false, false, false, "csv"), pheno(NULL), lambda(Lambda), wbeta(wBeta), wd(wD), wmu(wm), mbeta(mBeta), md(mD), mmu(mm), T(Tf), n(Nx), N(Nmax), shift_cpt(0), file_init_cond(""), output_at_exit(false), noshift(false), extra_verbose(false), spatial(false), step(0.1), a(0), b(10)
{
	// Parametres du modele
	Add("lambda", lambda);
	Add("wbeta", wbeta);
	Add("mbeta", mbeta);
	Add("N", N);
	Add("wd", wd);
	Add("md", md);
	Add("wmu", wmu);
	Add("mmu", mmu);
	Add("n", n);
	Add("T", T);
	
	// Parametres du programme
	Add("file_init_cond", file_init_cond);
	
	// Options controlling the spatial context
	Add("a", a);
	Add("b", b);
	Add("step", step);

	// Commandes
	Set("--noshift", "", noshift);
	Set("--extra_verbose", "never stops talking", extra_verbose);
	Set("--output_at_exit", "Print a summary on standard exit at the end", output_at_exit);
	Set("--spatial", "activate the spatial interpretation of the parameters. When this option is set, the value of n is overwritten and the value of lambda is adjusted to match the new spatial requirements.", spatial);
	
	SetHelp("a", "if the spatial context is set, defines the left corner of the space");
	SetHelp("b", "if the spatial context is set, defines the left corner of the space");
	SetHelp("step", "if the spatial context is set, defines the step of discretization.\n This parameter can be readjusted internally, so that the effective step is usually lower than this value.\n This parameter mostly defines an upper bound of the effective step.");
}

Experiment::~Experiment()
{
	if (pheno != NULL)
		delete pheno;
}

/*****************************************************************************
* Traite les messages                                                        *
*****************************************************************************/

bool Experiment::HandleEvent(size_t ev)
{
	switch(ev)
	{
		// Initialisation
		case EVENT_INIT:
		{
			// Recuperation des parametres du modele
			Get("lambda", lambda);
			Get("wbeta", wbeta);
			Get("mbeta", mbeta);
			Get("N", N);
			Get("wd", wd);
			Get("md", md);
			Get("wmu", wmu);
			Get("mmu", mmu);
			Get("n", n);
			Get("T", T);
			
			Get("a", a);
			Get("b", b);
			Get("step", step);
			
			// Recuperation des parametres du programme
			Get("file_init_cond", file_init_cond);
			
			// Recuperation des commandes
			GetCmd("--noshift", noshift);
			GetCmd("--extra_verbose", extra_verbose);
			GetCmd("--output_at_exit", output_at_exit);
			GetCmd("--spatial", spatial);
	
			// On definit la date de fin du programme
			evman.Push_Event(T, EVENT_EXIT);
			
			// Sets the spatial context if required
			if (spatial)
			{
				double length=b-a;
				
				// Compute the size of the array in memory
				n=size_t((length/step)+0.5)+1;
				
				step=length/(n-1); // recomputes the step to avoid misinterpretation
				
				// Export the new values of step and n for convenience
				
				SetVar("n", n);
				SetVar("step", step);
				
				lambda=lambda/(step*step); // Recomputes the rate of exchange
			}
			
			// Condition initiale pour la population
			bool StdInitCond=true;
			
			vector<size_t> winit_cond(n, 0);
			vector<size_t> minit_cond(n, 0);
			
			if (file_init_cond.size()>0)
			{
				ifstream finitcond;
				
				finitcond.open(file_init_cond.c_str(), fstream::in);
				
				if (finitcond.is_open())
				{
					string ic;
					size_t in;
					size_t i=0;
				
					while ((!finitcond.eof()) && (i < n))
					{
						{
							stringstream icstream;
							finitcond >> ic;
							icstream << ic;
							icstream >> in;
						}
						
						if (!finitcond.eof())
							winit_cond[i++]=in;
					}
					
					finitcond.clear();
									
					while ((!finitcond.eof()) && (i < n))
					{
						{
							stringstream icstream;
							finitcond >> ic;
							icstream << ic;
							icstream >> in;
						}
						
						if (!finitcond.eof())
							minit_cond[i++]=in;
					}
					
					finitcond.close();
					
					StdInitCond=false;
				}
				else
				{
					cerr << "Unable to read " << file_init_cond << " ; switching to standard initial condition" << endl;
				}
			}
				
			if (StdInitCond)
			{
				for (size_t i=0; i<n; i++)
				{
					winit_cond[i]=(i<=n/2) ? 1:0;
				}
			}
			
			// Conversion of the parameters
			lambda=lambda / N;

			// Creation of the main class
			pheno=new Modele_epidemio_adhoc(winit_cond, minit_cond, wbeta, wd, wmu, mbeta, md, mmu, lambda, N, noshift);
			
			// On initialise le moteur de Gillespie
			G=Gillespie(9);
			
			G.Set(0, pheno->GetPop(ID_WILDTYPE)*wd); 	// Wild type death
			G.Set(1, pheno->GetPop(ID_MUTANT)*md); 		// Mutant death
			G.Set(2, pheno->GetPairWithSusceptible(ID_WILDTYPE)*lambda); 	// Exchange wt<->S
			G.Set(3, pheno->GetPairWithSusceptible(ID_MUTANT)*lambda); 	// Exchange m<->S
			G.Set(4, pheno->GetExchange()*lambda); 		// Exchange wt<->m
			G.Set(5, wbeta*pheno->GetNewInfections(ID_WILDTYPE)/N);	// new wild type
			G.Set(6, mbeta*pheno->GetNewInfections(ID_MUTANT)/N);	// new mutant
			G.Set(7, pheno->GetPop(ID_WILDTYPE)*wmu); 	// Wild type mutation
			G.Set(8, pheno->GetPop(ID_MUTANT)*mmu); 		// Mutant mutation
			
			G.Update();
			
			// Envoi du premier evenement
			evman.Push_Event(G.GetNextTime(), EVENT_GILLESPIE);
			
			if (extra_verbose)
			{
				cout << "init terminated" << endl;
				cout << "pop : " << pheno->GetPop(ID_WILDTYPE) + pheno->GetPop(ID_MUTANT) << endl;
			}
		}
		break;
			
		case EVENT_GILLESPIE:
		{
			if (extra_verbose)
			{
				cout << "time : " << evman.GetTime() << endl;
				cout << "pop : " << pheno->GetPop(ID_WILDTYPE) + pheno->GetPop(ID_MUTANT) << endl;
			}
			
			// On selectionne l'evenement sur la population
			switch (G.GetNextEvent())
			{
				case 0: // KILL wild type
					if (extra_verbose)
						cout << "KILL WT" << endl;
					// pheno gere en interne la repartition
					if (!pheno->Kill(ID_WILDTYPE))
						cerr << evman.GetTime() << " : KILL failed " << endl;
					break;
					
				case 1: // KILL mutant
					if (extra_verbose)
						cout << "KILL M" << endl;
					// pheno gere en interne la repartition
					if (!pheno->Kill(ID_MUTANT))
						cerr << evman.GetTime() << " : KILL failed " << endl;
					break;
				
				case 2: // MOVE wild type
					if (extra_verbose)
						cout << "MOVE WT" << endl;
						
					if (!pheno->Move(ID_WILDTYPE))
						cerr << evman.GetTime() << " : MOVE failed " << endl;
					break;
					
				case 3: // MOVE mutant
					if (extra_verbose)
						cout << "MOVE M" << endl;
						
					if (!pheno->Move(ID_MUTANT))
						cerr << evman.GetTime() << " : MOVE failed " << endl;
					break;
					
				case 4: // EXCHANGE
					if (extra_verbose)
						cout << "EXCHANGE" << endl;
						
					if (!pheno->Exchange())
						cerr << evman.GetTime() << " : EXCHANGE failed" << endl;
					break;
					
				case 5: // NEW_INF wild type
					if (extra_verbose)
						cout << "NEW_INF WT" << endl;
						
					if (!pheno->NewInfection(ID_WILDTYPE))
						cerr << evman.GetTime() << " : NEW_INF failed" << endl;
					break;
					
				case 6: // NEW_INF wild type
					if (extra_verbose)
						cout << "NEW_INF M" << endl;
						
					if (!pheno->NewInfection(ID_MUTANT))
						cerr << evman.GetTime() << " : NEW_INF failed" << endl;
					break;
					
				case 7: // MUT wild_type
					if (extra_verbose)
						cout << "MUT WT" << endl;
						
					if (!pheno->Mutate(ID_WILDTYPE))
						cerr << evman.GetTime() << " : MUT failed" << endl;
						
					break; 
					
				case 8: // MUT mutant
					if (extra_verbose)
						cout << "MUT M" << endl;
						
					if (!pheno->Mutate(ID_MUTANT))
						cerr << evman.GetTime() << " : MUT failed" << endl;
					break;
			}
			
			if (!noshift)
			{
				// Shift de la pop si elle va trop loin
				if (pheno->operator[](ID_WILDTYPE)[n-1] + pheno->operator[](ID_MUTANT)[n-1]>0)
				{
					pheno->Shift();
					shift_cpt++;
				}
				
				if (extra_verbose)
				{
					cout << "cpt : " << shift_cpt << endl;
				}
			}
			
			pheno->All();
			
			// Mise a jour du moteur de Gillespie
			if (pheno->GetPop(ID_WILDTYPE)+pheno->GetPop(ID_MUTANT) > 0)
			{
				// Mise a jour des evenements
		
				G.Set(0, pheno->GetPop(ID_WILDTYPE)*wd); 	// Wild type death
				G.Set(1, pheno->GetPop(ID_MUTANT)*md); 		// Mutant death
				G.Set(2, pheno->GetPairWithSusceptible(ID_WILDTYPE)*lambda); 	
				// Exchange wt<->S
				G.Set(3, pheno->GetPairWithSusceptible(ID_MUTANT)*lambda); 	
				// Exchange m<->S
				G.Set(4, pheno->GetExchange()*lambda); 		// Exchange wt<->m
				G.Set(5, wbeta*pheno->GetNewInfections(ID_WILDTYPE)/N);	// new wild type
				G.Set(6, mbeta*pheno->GetNewInfections(ID_MUTANT)/N);	// new mutant
				G.Set(7, pheno->GetPop(ID_WILDTYPE)*wmu); 	// Wild type mutation
				G.Set(8, pheno->GetPop(ID_MUTANT)*mmu); 		// Mutant mutation

				// Recalcul de la somme des taux pour eviter les erreurs d'arrondi
				G.Update();
				
				// Envoi de l'evenement suivant
				evman.Push_Event(evman.GetTime() + G.GetNextTime(), EVENT_GILLESPIE);
			}
		}
		break;
		
		case EVENT_AT_EXIT:
		{
			if (output_at_exit)
			{
				// Affichage de l'etat final et la population en commentaire
				cout << "# Population totale : " << pheno->GetPop(ID_WILDTYPE) + pheno->GetPop(ID_MUTANT) << endl;
				cout << *pheno << endl;
			}
		}
		break;
		
		default:
			cerr << "Unexpected event" << endl;
			return false;
	}
	
	return true;
}

/*****************************************************************************
* Initialise les echantillons                                                *
*****************************************************************************/

bool Experiment::SampleInit(SimpleXMLTree & temp, SimpleXMLTree & tp)
{
	ofstream out;
	bool ret=true;


	SimpleXMLTree * tmp=new SimpleXMLTree("parameters");
	
	GetVarXML(*tmp);
	
	tp.Append(tmp);
		
	tmp=new SimpleXMLTree("commands");
	
	if (GetCmdXML(*tmp))
		tp.Append(tmp);
	
	// Creates template of sample
	temp.Append(new SimpleXMLTree("time", "", 0));
	
	temp.Append(new SimpleXMLTree("shift", "", 0));
		
	temp.Append(new SimpleXMLTree("wildtype", "", 0));

	temp.Append(new SimpleXMLTree("mutant", "", 0));
	
	return ret;
}

/*****************************************************************************
* Echantillonnage                                                            *
*****************************************************************************/

bool Experiment::Sample(SimpleXMLTree & tp)
{
	ofstream out;
	bool ret=true;
	
	ostringstream txt;
	string str_txt;
		
	txt << evman.GetTime();
	str_txt=txt.str();
	
	txt.str("");
	
	tp.GetChild(tp.find("time"))->GetText()=str_txt;
	
	// sample the state	
		str_txt="";
		
		for (size_t i=0; i<pheno->operator[](ID_WILDTYPE).size(); i++)
		{
			string tmpstr;
			
			txt << pheno->operator[](ID_WILDTYPE)[i];
			tmpstr=txt.str();
			
			txt.str("");
			
			str_txt+=tmpstr + " ";
		}
		
		tp.GetChild(tp.find("wildtype"))->GetText()=str_txt;
		
		str_txt="";
		
		for (size_t i=0; i<pheno->operator[](ID_MUTANT).size(); i++)
		{
			string tmpstr;
			
			txt << pheno->operator[](ID_MUTANT)[i];
			tmpstr=txt.str();
			
			txt.str("");
			
			str_txt+=tmpstr + " ";
		}
		
		tp.GetChild(tp.find("mutant"))->GetText()=str_txt;
	
	// Sample the shift
	{
		str_txt="";
		txt.str("");
		
		txt << shift_cpt;
					
		tp.GetChild(tp.find("shift"))->GetText()=txt.str();
	}

	return ret;
}

/*****************************************************************************
* Echantillonnage final                                                      *
*****************************************************************************/

bool Experiment::SampleAtExit(SimpleXMLTree & tp)
{
	bool ret=true;
	ofstream out;
	
	ret &= Sample(tp);
	
	return ret;
}

