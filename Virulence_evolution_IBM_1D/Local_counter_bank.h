/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Local_counter_bank.h                                                       *
* Calculates and keep local-dependent global functions on an array           *
*****************************************************************************/

#ifndef __LOCAL_COUNTER_BANK_H__
	#define __LOCAL_COUNTER_BANK_H__

#include "Counter.h"
#include "Payoff.h"

using namespace std;

template<typename T, size_t N, typename S=T>
class Local_counter_bank:public Counter<T, N>
{
	protected:
		vector<Payoff<S, T> *> countfun;
			
	public:
		Local_counter_bank() {};
		Local_counter_bank(vector<T> init) : Counter<T,N> (init)  { };
		Local_counter_bank(vector<T> init [] ):Counter<T,N> (init)  { };
		Local_counter_bank(const Local_counter_bank & cp) :Counter<T, N>(cp), countfun(cp.countfun) {};
virtual		~Local_counter_bank() {};
/*		
Inherited from Counter functions : 
		const vector<T> &	operator [] (size_t i) const;
		T &			operator () ( size_t i, size_t j);
		bool 			Waiting() const;
virtual		bool			Next();	
		void 			All();
Member functions : 
		void AddPayoff(Payoff<S, T> * p) ;
		T GetPayValue(size_t i);
virtual		bool Next();
*/
		
inline		void AddPayoff(Payoff<S, T> * p) { countfun.push_back(p); };
inline		S GetPayValue(size_t i)  
		{ 
			if (i >= countfun.size()) 
				return 0; 
			else 
			{
				if (Local_counter_bank<T, N, S>::w.size()==0)
					return countfun[i]->GetResult(); 
				else
				{
					Local_counter_bank<T, N, S>::All();
					return countfun[i]->GetResult(); 
				}
			}
		}; 
		bool Next();
};	
	
template<typename T, size_t N, typename S>
bool 	Local_counter_bank<T, N, S>::Next()
{
	if (Counter<T, N>::w.size() > 0)
	{
		for (size_t i=0; i<countfun.size(); i++)
		{
			countfun[i]->Calc(&Local_counter_bank<T, N, S>::c[0], Local_counter_bank<T, N, S>::w.front().first.first, Local_counter_bank<T, N, S>::w.front().first.second, Local_counter_bank<T, N, S>::w.front().second);
		}
	
		Local_counter_bank<T, N, S>::Execute();
		
		Local_counter_bank<T, N, S>::w.pop();
		
		return true;
	}
	else
	{
		return false;
	}
}
	
#endif

