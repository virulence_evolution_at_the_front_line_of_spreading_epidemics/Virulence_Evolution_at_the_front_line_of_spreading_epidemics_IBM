#!/bin/bash

#    This program does IBM simulations for an epidemiological model
#    
#    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
#    
#    This file is part of Virulence_evolution_IBM_1D.
#
#    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
#    
#    Contact : Quentin Griette <q.griette@gmail.com>




g++ -lgsl -lgslcblas -Wall -o Virulence_evolution_IBM_1D Modele_epidemio_adhoc.cpp main.cpp Experiment.cpp Utilities/Engine_event_sample_base.cpp Utilities/Engine_event_XMLsample_base.cpp Utilities/Engine_event_base.cpp Utilities/Engine_base.cpp Utilities/XML/SimpleXMLTree.cpp  Utilities/User_interaction/Cmd_man.cpp Utilities/User_interaction/Var_man.cpp Utilities/Random/Alea.cpp Utilities/Random/Gillespie.cpp 
