/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

#ifndef __EXPERIMENT_H__
	#define __EXPERIMENT_H__
	
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#include "Modele_epidemio_adhoc.h"
#include "Utilities/Random/Gillespie.h"
#include "Utilities/Engine_event_XMLsample_base.h"

#define EVENT_GILLESPIE 		5
#define EVENT_START_SPEED_SAMPLE 	6
#define EVENT_SAMPLE_SPEED		7

class Experiment : public Engine_event_XMLsample_base
{
	protected:
		Modele_epidemio_adhoc * pheno; // La population
		Gillespie G; // Pour gerer l'aleatoire
		
		double lambda, wbeta, wd, wmu, mbeta, md, mmu, T;	// Parametres du modele
		size_t n, N;						// Parametres du modele
		size_t shift_cpt;					// Parametres internes
		string file_init_cond;					// Import the initial condition from this file	
		
		bool output_at_exit;
		bool noshift, extra_verbose;				// Parametres du programme
		
		bool spatial;						// Option controling the parameters interpretation
		double step, a, b;					// In case the spatial command is true, these parameters control the spatial context

virtual		bool HandleEvent(size_t ev);				// Gestion des evenements
		
virtual		bool SampleInit(SimpleXMLTree &, SimpleXMLTree &);	// Initialisation des echantillons
virtual		bool Sample(SimpleXMLTree &);				// Echantillonnage
virtual		bool SampleAtExit(SimpleXMLTree &);					// Echantillonnage de sortie
		
	public:	
		Experiment(double Lambda=1.0, double wBeta=4.0, double wD=1.0, double wm=0.01, double mBeta=50.0, double mD=40.0, double mm=0.01, size_t Nx=100, size_t Nmax=100, double Tf=10);
virtual		~Experiment();		
};
	
#endif

