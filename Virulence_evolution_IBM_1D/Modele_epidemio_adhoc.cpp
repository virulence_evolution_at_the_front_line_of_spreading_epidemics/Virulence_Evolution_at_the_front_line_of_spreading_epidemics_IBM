/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Modele_epidemio_adhoc.cpp                                                  *
* Self-explaining : this model is optimized for one purppse only             *
* This one codes a SI model at population equilibrium                        *
* note that the susceptible hosts are implicit (I+S=N)                       *
*****************************************************************************/

#include "Modele_epidemio_adhoc.h"

Modele_epidemio_adhoc::Modele_epidemio_adhoc(vector<size_t> winit, vector<size_t> minit, double wb, double wdr, double wm, double mb, double mdr, double mm, double l, size_t n, bool ns)
	:Local_counter_bank<size_t, 2, size_t>(vector<size_t>(winit.size(), 0)), wbeta(wb), wd(wdr), wmu(wm), mbeta(mb), md(mdr), mmu(mm), lambda(l), N(n), shift_cpt(0), wpop(ID_WILDTYPE), wsquares(ID_WILDTYPE), wmove(ID_WILDTYPE), mpop(ID_MUTANT), msquares(ID_MUTANT), mmove(ID_MUTANT), crossp(ID_WILDTYPE, ID_MUTANT), scalarp(ID_WILDTYPE, ID_MUTANT), noshift(ns)
{
	AddPayoff(&wpop);
	AddPayoff(&mpop);
	AddPayoff(&wsquares);
	AddPayoff(&msquares);
	AddPayoff(&wmove);
	AddPayoff(&mmove);
	AddPayoff(&crossp);
	AddPayoff(&scalarp);
	
	for (size_t i=0; i<winit.size(); i++)
	{
		this->operator()(ID_WILDTYPE, i)=winit[i];
		Next();
	}
	
	for (size_t i=0; i<minit.size(); i++)
	{
		this->operator()(ID_MUTANT, i)=minit[i];
		Next();
	}
	
	All(); 			// Empty the stack, just to be sure
}		
	
void Modele_epidemio_adhoc::Shift() 		// Shift everybody to the right or left
						// One value is lost
						// The other informations left are updated
{
	this->operator()(ID_WILDTYPE, 0)=0; // Destroy first value
	this->operator()(ID_MUTANT, 0)=0; // Destroy first value
	
	All(); // Update payoffs
		
	for (size_t i=0; i<c[ID_WILDTYPE].size() - 1; i++) // Actual shift
	{
		c[ID_WILDTYPE][i]=c[ID_WILDTYPE][i+1];
		c[ID_MUTANT][i]=c[ID_MUTANT][i+1];
	}
	c[ID_WILDTYPE][c[ID_WILDTYPE].size()-1]=0;
	c[ID_MUTANT][c[ID_MUTANT].size()-1]=0;
	
	shift_cpt++;
}
	
bool Modele_epidemio_adhoc::Kill(size_t who) 	// Kill one wild type or one mutant, depending on
						// variable who
{
	size_t x=MyAlea::a.Uniform(GetPayValue(ID_RES_POP + who)) + 1;
	size_t i=0, sum=0;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i++];
	}
	
	i--;
	
	if (this->operator[](who)[i]>0)
	{
		this->operator()(who, i)=this->operator[](who)[i]-1; 	// Schedule reduction in population
		
		return true;
	}
	else
		return false;
}

bool Modele_epidemio_adhoc::Mutate(size_t who) 	// Kill one wild type or one mutant, depending on
						// variable who
{
	size_t x=MyAlea::a.Uniform(GetPayValue(ID_RES_POP+who)) + 1;
	size_t i=0, sum=0;
	size_t the_other=(who+1) % 2;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i++];
	}
	
	i--;
	
	if ((this->operator[](who)[i]>0)&&(this->operator[](the_other)[i]<N))
	{
		this->operator()(who, i)=this->operator[](who)[i]-1; 	// Schedule reduction in population
		this->operator()(the_other, i)=this->operator[](the_other)[i]+1; 	// Schedule increase in population
		
		return true;
	}
	else
		return false;
}

bool Modele_epidemio_adhoc::Move(size_t who)	// move from one column to another with uniform probability on
					// (infected(i) \times susceptible(i+inc))
{
	size_t total=GetPairWithSusceptible(who);
	size_t x=MyAlea::a.Uniform(total) + 1;
	size_t i=c[who].size() - 1, sum=0;
	int inc=-1;
		
	while (x > sum)
	{
		size_t num=this->operator[](who)[i];
		
		if (i==0)
		{
			sum+=num*(N-this->operator[](ID_WILDTYPE)[1]-this->operator[](ID_MUTANT)[1]);
		}
		else
		{
			if (i==c[who].size() - 1)
			{
				sum+=num*(N-this->operator[](ID_WILDTYPE)[i-1]-this->operator[](ID_MUTANT)[i-1]);
			}
			else
			{
				sum+=num*(N - this->operator[](ID_WILDTYPE)[i+inc]-this->operator[](ID_MUTANT)[i+inc]);
			}
		}
		
		if (inc==-1)
			i--;
		inc=-inc;
	}
	
	inc=-inc;
	if (inc==-1)
		i++;
	
	
	// Schedule move
	if ((this->operator[](who)[i]>0) && (this->operator[](who)[i+inc]<N))
	{
		this->operator()(who, i)=this->operator[](who)[i]-1;
		this->operator()(who, i+inc)=this->operator[](who)[i+inc]+1;
		
		return true;
	}
	else
		return false;
}
	
bool Modele_epidemio_adhoc::NewInfection(size_t who) 	// Convert a susceptible into an infected host.
{
	size_t total=GetNewInfections(who);
	size_t x=MyAlea::a.Uniform(total) + 1;
	size_t sum=0;
	size_t i=this->operator[](who).size() - 1;

	while (x>sum) 	// find the right element
	{
		sum+=this->operator[](who)[i]*(N-this->operator[](ID_WILDTYPE)[i]-this->operator[](ID_MUTANT)[i]);
		i--;
	}
	
	i++;
	
	if (this->operator[](who)[i] < N)
	{
		this->operator()(who, i)=this->operator[](who)[i]+1; 	// Schedule increase in population

		return true;
	}
	else
		return false;
}	

bool Modele_epidemio_adhoc::Exchange()
{
	size_t total=GetExchange();
	size_t x=MyAlea::a.Uniform(total) + 1;
	size_t sum=0;
	size_t i=this->operator[](ID_WILDTYPE).size()-1;
	while (sum < x)
	{
		sum +=this->operator[](ID_WILDTYPE)[i-1]*this->operator[](ID_MUTANT)[i]+this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i-1];
		i--;
	}
	
	i++;
	
	sum -= this->operator[](ID_WILDTYPE)[i]*this->operator[](ID_MUTANT)[i-1];
	
	if (x>sum)
	{
		if ((this->operator[](ID_WILDTYPE)[i]>0) && (this->operator[](ID_WILDTYPE)[i-1]<N))
		{
			this->operator()(ID_WILDTYPE, i)=this->operator[](ID_WILDTYPE)[i]-1;
			this->operator()(ID_WILDTYPE, i-1)=this->operator[](ID_WILDTYPE)[i-1]+1;
		}
		else
			return false;
			
		if ((this->operator[](ID_MUTANT)[i-1]>0) && (this->operator[](ID_MUTANT)[i]<N))
		{
			this->operator()(ID_MUTANT, i-1)=this->operator[](ID_MUTANT)[i-1]-1;
			this->operator()(ID_MUTANT, i)=this->operator[](ID_MUTANT)[i]+1;
		}
		else
			return false;
			
		return true;
	}
	else
	{
		if ((this->operator[](ID_WILDTYPE)[i-1]>0) && (this->operator[](ID_WILDTYPE)[i]<N))
		{
			this->operator()(ID_WILDTYPE, i)=this->operator[](ID_WILDTYPE)[i]+1;
			this->operator()(ID_WILDTYPE, i-1)=this->operator[](ID_WILDTYPE)[i-1]-1;
		}
		else
			return false;
			
		if ((this->operator[](ID_MUTANT)[i]>0) && (this->operator[](ID_MUTANT)[i-1]<N))
		{
			this->operator()(ID_MUTANT, i-1)=this->operator[](ID_MUTANT)[i-1]+1;
			this->operator()(ID_MUTANT, i)=this->operator[](ID_MUTANT)[i]-1;
		}
		else
			return false;
			
		return true;
	}
}
	
		
	
