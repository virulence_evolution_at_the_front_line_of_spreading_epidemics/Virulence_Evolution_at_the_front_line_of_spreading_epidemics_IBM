/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Counter.h                                                                  *
* Dynamical array that behaves like a counter. It keeps track of any update. *
*****************************************************************************/

#ifndef __COUNTER_H__
	#define __COUNTER_H__
	
#include <vector>
#include <queue>

using namespace std;

template<typename T, size_t N = 1> // T : elements type; N : number of communicating counters
class Counter
{
	protected:
		vector<T> 				c [N]; 	// counters
		queue<pair<pair<size_t, size_t>, T> >	w;	// waiting customers
		T 					absorb;
		
		bool 	Execute();
		
	public:
		Counter() { };
		Counter(vector<T> init) { for (size_t i=0; i<N; i++) c[i]=init; };
		Counter(vector<T> init [] ) { for (size_t i=0; i<N; i++) c[i]=init[i]; };
		Counter(const Counter<T, N> & cp) { for (size_t i=0; i<N; i++) { c[i]=cp.c[i]; w=cp.w; } };
virtual		~Counter() {};

/*		Member functions : 
		const vector<T> &	operator [] (size_t i) const;
		T &			operator () ( size_t i, size_t j);
		bool 			Waiting() const;
		bool			Next();	
		void 			All();
*/

inline		const vector<T> &	operator [] (size_t i) const { return c[i]; }; // read only vector
inline		T &			operator () (size_t i, size_t j) // Push operation (customer)
		{
			if (i>=N)
			{
				return absorb;
			}
			else
			{
				if ( i >= c[i].size() )
				{
					return absorb; 
				}
				else
				{
					w.push(pair<pair<size_t, size_t>, T>(pair<size_t, size_t>(i, j), this->operator[](i)[j]));
						
					return w.back().second; 
				}
			}
		};
		
inline		bool	Waiting() const	{ return (w.size() > 0); }; // Is there anybody waiting
virtual		bool	Next() { if (Execute()) { w.pop(); return true; } else return false; };		// serve next customer
inline		void 	All() { while (Next()); };	// serve everybody waiting
};
	
template<typename T, size_t N>
bool	Counter<T, N>::Execute()
{
	if (w.size() > 0)
	{
		c[w.front().first.first] [w.front().first.second]=w.front().second;
		
		return true;
	}
	else
		return false;
}

#endif
	
