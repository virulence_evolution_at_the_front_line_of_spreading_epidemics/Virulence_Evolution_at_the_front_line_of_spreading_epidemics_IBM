/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Modele_epidemio_adhoc.cpp                                                  *
* Self-explaining : this model is optimized for one purppse only             *
* This one codes a SI model at population equilibrium                        *
* note that the susceptible hosts are implicit (I+S=N)                       *
*****************************************************************************/

#ifndef __MODELE_EPIDEMIO_ADHOC_H__
	#define __MODELE_EPIDEMIO_ADHOC_H__
	
#include <iostream>

using namespace std;

#include "Local_counter_bank.h"
#include "Ad_hoc_payoffs.h"

#include "Utilities/Random/Alea.h"

#define ID_WILDTYPE 0
#define ID_MUTANT 1

#define ID_RES_POP 0
#define ID_RES_WPOP 0
#define ID_RES_MPOP 1
#define ID_RES_SQUARES 2
#define ID_RES_WSQUARES 2
#define ID_RES_MSQUARES 3
#define ID_RES_MOVE 4
#define ID_RES_WMOVE 4
#define ID_RES_MMOVE 5

#define ID_RES_CROSSP 6
#define ID_RES_SCALARP 7

class Modele_epidemio_adhoc : public Local_counter_bank<size_t, 2, size_t>
{
	protected:
		double wbeta, wd, wmu;		// parameters
		double  mbeta, md, mmu;		// parameters
		
		double lambda;
		size_t N; 			// Total pop (infected+susceptibles)
		
		size_t shift_cpt;			// current shift from the initial position
		
		paySum_oneline<size_t, size_t> 		wpop; 		// Population of wild type
		paySqSum_oneline<size_t, size_t>	wsquares; 	// Sum of squares, wild type
		payLocalExchange<size_t, size_t>	wmove;

		paySum_oneline<size_t, size_t> 		mpop; 		// Population of mutant
		paySqSum_oneline<size_t, size_t>	msquares; 	// Sum of squares, mutant
		payLocalExchange<size_t, size_t>	mmove;
		
		payLocalCross<size_t, size_t>		crossp;		// cross product 
		payScalarP<size_t, size_t>		scalarp;	// scalar product
		
		bool	noshift; 					// Self-explaining

	public:	
		Modele_epidemio_adhoc(vector<size_t> init, vector<size_t> minit, double wb, double wdr, double wm, double mb, double mdr, double mm, double l, size_t n, bool ns=false);
virtual 	~Modele_epidemio_adhoc() {};
		
inline		size_t GetPop(size_t who) { return GetPayValue(ID_RES_POP + who); };
		
		void 	Shift();
		
		bool Kill(size_t who);
		bool Mutate(size_t who);
		bool Move(size_t who);
		bool NewInfection(size_t who);
		bool Exchange();
		
inline		size_t	GetPairWithSusceptible(size_t who)
		{
			return N*(2*GetPayValue(ID_RES_POP + who)-this->operator[](who)[0]-this->operator[](who)[this->operator[](who).size()-1])-2*GetPayValue(ID_RES_MOVE + who)-GetPayValue(ID_RES_CROSSP);
		};
		
inline		size_t 	GetNewInfections(size_t who)
		{
			return N*GetPayValue(ID_RES_POP + who)-GetPayValue(ID_RES_SQUARES + who)-GetPayValue(ID_RES_SCALARP);
		}
		
inline		size_t GetExchange()
		{
			return GetPayValue(ID_RES_CROSSP);
		}
		
friend		ostream & operator<<(ostream & out, Modele_epidemio_adhoc in)
{
	out << "wild type" << endl;
	for (size_t i=0; i<in.c[ID_WILDTYPE].size(); i++)
	{	
		out << in.c[ID_WILDTYPE][i] << " ";
	}
	out << endl;
	
	out << "mutant" << endl;
	for (size_t i=0; i<in.c[ID_MUTANT].size(); i++)
	{	
		out << in.c[ID_MUTANT][i] << " ";
	}
	out << endl;

	return out;
};
	
};	
#endif

