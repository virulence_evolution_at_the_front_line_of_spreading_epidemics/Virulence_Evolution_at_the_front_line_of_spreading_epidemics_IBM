/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Payoff.h                                                                   *
* Abstract class for array-friendly payoff functions                         *
*****************************************************************************/

#ifndef __PAYOFF_H__
	#define __PAYOFF_H__

#include <iostream>
using namespace std;

template<typename T, typename S=T>
class Payoff
{
	protected:
		T result;

	public:
		Payoff() {};
		Payoff(const T & r) :result(r) { };
		Payoff(const Payoff<T, S> & cp) :result(cp.result) {};
		
		~Payoff() {};
		
inline		T	GetResult() const { return result; };
inline		void	SetResult(const T & n) { result=n; };
				
virtual		void	Calc(const vector<S>* counters, size_t counter, size_t desk, const S & newval)=0;
};

#endif

