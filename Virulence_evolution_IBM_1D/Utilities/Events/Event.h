/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Event.h                                                                    *
* Evenement (ad hoc)                                                         *
*****************************************************************************/

#ifndef __EVENT_H__
	#define __EVENT_H__
	
#include <cstdlib>

template<typename corpus_type> class Event_Stack;

template<typename corpus_type>	
class Event // Typiquement, une liste chainee ordonnee par le temps
{
	protected:
		double 			        time;
		corpus_type 		    corpus;
		Event<corpus_type> *	next;
		
	public:
		Event(); // par defaut : ne fait rien.
		Event(double t, const corpus_type & ev, Event * next=NULL); // copie ev
		Event(const Event &); // copie l'ensemble
		~Event();
		
		double 			    GetTime() 	    const { return time; };
		const corpus_type	GetCorpus() 	const { return corpus; };
		const Event * 		GetNext() 	    const { return next; };
				
		void	Insert(Event *); // insere dans l'ordre chronologique
		void 	Eject()		{ next=NULL; }; // sort de la liste (next=NULL)
		
		friend class Event_Stack<corpus_type>;
};
	
template <typename corpus_type>
Event<corpus_type>::Event()
	:time(0), corpus(), next(NULL)
{}

template<typename corpus_type>
Event<corpus_type>::Event(double t, const corpus_type & c, Event * ne)
	:time(t), corpus(c), next(ne)
{
}

template<typename corpus_type>
Event<corpus_type>::Event(const Event & e)
	:time(e.time), corpus(e.corpus), next(e.next)
{
	
}

template<typename corpus_type>
Event<corpus_type>::~Event()
{
	if (next != NULL)
		delete next;
}

template<typename corpus_type>
void Event<corpus_type>::Insert(Event * e)
{
	if (e!=NULL)
	{
		if (next==NULL)
			next=e;
		else
		{
			// Insertion en queue dans l'ordre
			// Attention au premier element
			if (next->time > e->time)
			{
				e->next=next;
				next=e;
			}
			else
			{
				// Recursion
				next->Insert(e);
			}
		}
	}
}

#endif

