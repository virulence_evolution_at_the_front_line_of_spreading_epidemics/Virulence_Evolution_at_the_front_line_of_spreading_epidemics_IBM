/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_event_base.h                                                        *
* Engine with integrated events handler                                      *
*****************************************************************************/

#ifndef __ENGINE_EVENT_BASE_H__
	#define __ENGINE_EVENT_BASE_H__

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Engine_base.h"
#include "Events/Events_manip.h"	
	
#define EVENT_EXIT 	0
#define EVENT_INIT 	1
#define EVENT_AT_EXIT 	2

class Engine_event_base:public Engine_base
{
	private:		
		// file to read events from
		string 	events_file;
		// Read from keyboard
		bool events_from_cin;
		
virtual		int Internal_loop_0(); // Main loop
		
	protected:
		// Stack of events
		Events_manip<size_t> evman;
		// Events handler
virtual		bool HandleEvent_0(size_t event)=0;
		
	public:	
		Engine_event_base(string ef=string(""), bool fc=false, bool h=false, bool v=false);
		
virtual		~Engine_event_base() {};

};	
	
#endif

