/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_event_sample_base.h                                                 *
* Classe de base pour un moteur de gestion des evenements                    *
* Preconfiguration d'enregistrements reguliers                               *
*****************************************************************************/

#ifndef __ENGINE_EVENT_SAMPLE_BASE_H__
	#define __ENGINE_EVENT_SAMPLE_BASE_H__

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include "Engine_event_base.h"
	
#define EVENT_SAMPLE 	3

class Engine_event_sample_base:public Engine_event_base
{
	private:		
		double	sample_every;
		bool 	sample_at_exit;
				
	protected:
		// Fonction a surcharger pour gerer les evenements
inline virtual	bool HandleEvent_0(size_t event);
virtual		bool HandleEvent(size_t event);
		// Fonctions a surcharger pour les enregistrements
virtual		bool SampleInit();
virtual		bool Sample();
virtual		bool SampleAtExit();

	public:	
		Engine_event_sample_base(double se=0, bool sae=false, string ef=string(""), bool fc=false, bool h=false, bool v=false);
		
virtual		~Engine_event_sample_base() {};

};

/*****************************************************************************
* Gestion des evenements et echantillonnage                                  *
*****************************************************************************/
	
inline bool Engine_event_sample_base::HandleEvent_0(size_t event)
{
	bool ret=true;
	
	switch (event)
	{
		case EVENT_INIT:
			Get("sample_every", sample_every);
			GetCmd("--sample_at_exit", sample_at_exit);
			
			ret &= HandleEvent(event);
			
			ret &= SampleInit();
			
			if (sample_every != 0)
			{
				evman.Push_Event(sample_every, EVENT_SAMPLE);
			}
			
			break;
		
		case EVENT_SAMPLE:
			ret &= Sample();
			
			if (Verbose() && (!ret))
			{
				cerr << evman.GetTime() << "Something went wrong while sampling" << endl;
			}
			
			if (sample_every != 0)
			{
				evman.Push_Event(evman.GetTime() + sample_every, EVENT_SAMPLE);
			}
			
			break;
			
		case EVENT_AT_EXIT:
			if (sample_at_exit)
				ret &= SampleAtExit();
			
			if (Verbose() && (!ret))
				cerr << "An error occured with the final sampling" << endl;
				
			ret &= HandleEvent(event);
			
			break;
			
		default:
			HandleEvent(event);
	}
	
	return ret;
}

#endif

