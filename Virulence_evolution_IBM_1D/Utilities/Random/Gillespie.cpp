/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Gillespie.cpp                                                              *
* Implementation de la methode de Gillespie                                  *
*****************************************************************************/

#include "Gillespie.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Gillespie::Gillespie() // Par defaut : initialise tout a 0
	:sum(0)
{
}

Gillespie::Gillespie(size_t N) // Initialise N etats a 0
	:taux(N, 0), sum(0)
{
}

Gillespie::Gillespie(const vector<double> & t) // Initialise les etats a t
	:taux(t)
{
	Update();
}

Gillespie::Gillespie(const Gillespie & cp) // Copie
	:taux(cp.taux), sum(cp.sum)
{
}

/*****************************************************************************
* Mutateur                                                                   *
*****************************************************************************/

bool Gillespie::Set(size_t i, double t)
{
	 if ((i < taux.size()) && (t>=0))
	 {
	 	sum-=taux[i];
	 	taux[i]=t;
	 	sum+=taux[i];
	 	
	 	return true;
	 }
	 else
	 	return false;
}

/*****************************************************************************
* change toutes les probas                                                   *
*****************************************************************************/

bool Gillespie::Update(vector<double> & t)
{
	bool ret=true;
	double temp_sum=0;
	
	for (size_t i=0; i<t.size(); i++)
	{
		ret &= (t[i]>=0);
		temp_sum += t[i];
	}
	
	if (ret)
	{
		taux=t;
		sum=temp_sum;
		
		return true;
	}
	else
		return false;
}

/*****************************************************************************
* Recalcule la somme des taux                                                *
*****************************************************************************/

void Gillespie::Update()
{
	sum=0;
	
	for (size_t i=0; i<taux.size(); i++)
	{
		sum+=taux[i];
	}
}
	
/*****************************************************************************
* Calcule l'evenement suivant avec proba proportionnelle au taux             *
*****************************************************************************/

size_t	Gillespie::GetNextEvent() const
{
	// v.a. uniforme
	double x=MyAlea::a.X();
	
	double temp_sum=0;
	size_t ev=0;
	
	if (taux.size()==0)
	{
		return 0;
	}
	else
	{
		// Proba sur les evenements proportionnelle au taux
		temp_sum=taux[0]/sum;
		
		while (x>temp_sum)
		{
			ev++;
			temp_sum += taux[ev]/sum;
		}
		
		return ev;
	}
}

/*****************************************************************************
* Calcule la date du prochain evenement                                      *
*****************************************************************************/

double 	Gillespie::GetNextTime() const	
{
	return MyAlea::a.Exp(sum);
}

