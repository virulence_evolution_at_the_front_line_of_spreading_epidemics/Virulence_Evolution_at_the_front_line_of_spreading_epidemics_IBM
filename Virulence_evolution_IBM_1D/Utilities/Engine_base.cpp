/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_base.cpp                                                            *
* Classe de base pour un moteur de programme                                 *
*****************************************************************************/

#include "Engine_base.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Engine_base::Engine_base(bool h, bool v)
	:help(h), verbose(v)
{
	// Configuration du mode "aide"
	Set("--help", "Print this message (+ --verbose : current value of the parameters)",  help);
	// verbose sera utile dans les classes derivees
	Set("--verbose", "more chatty than the usual", verbose);
}

/*****************************************************************************
* Recuperation des arguments passes en ligne de commande                     *
*****************************************************************************/

bool Engine_base::Start(size_t argc, const char * argv [])
{
	bool ret=true;
	
	if (argc > 1)
	{
		for (size_t i= 1; i < argc; i++)
		{
			bool flag=false;
			
			flag|=handle(argv[i]);
			
			if (!flag)
			{
				cerr << "Argument incorrect : " << argv[i] << endl;
				cerr << "Pour obtenir de l'aide : " << argv[0] << " --help" << endl;
			}
			
			ret &= flag;
		}
		
		return ret;
	}
	else
	{
		return true;
	}
}


/*****************************************************************************
* Boucle principale                                                          *
*****************************************************************************/

int Engine_base::Loop()
{
	// Recuperation des informations
	GetCmd("--help", help);
	GetCmd("--verbose", verbose);
	
	// Si l'option --help est active, on affiche l'aide et on quitte.
	if (help)
	{
		if (verbose) // Si verbose, on affiche les valeurs des parametres
			Display_help(cout, true);
		else
			Display_help(cout, false);
			
		return 0;
	}
	else // Sinon on passe a la boucle interne
	{
		return Internal_loop_0();
	}
}

