/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Classe generique pour la gestion de parametres passes en ligne de          *
* commande                                                                   *
* Les parametres les plus courants d'arguments sont pris en compte.          *
*****************************************************************************/

#include "Var_man.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Var_man::Var_man(string s_a)
	:seq_affect(s_a)
{
}

/*****************************************************************************
* Accesseurs                                                                 *
*****************************************************************************/

bool Var_man::GetIntValue(const string & s, int & ret_value) const
{
    map<string, int>::const_iterator it;
    it = entiers.find(s);

    if (it != entiers.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

bool Var_man::GetSize_tValue(const string &s, size_t &ret_value) const
{
    map<string, size_t>::const_iterator it;
    it = type_taille.find(s);

    if (it != type_taille.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

bool Var_man::GetFloatValue(const string &s, float & ret_value) const
{
    map<string, float>::const_iterator it;
    it = flottants.find(s);

    if (it != flottants.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

bool Var_man::GetDoubleValue(const string &s, double & ret_value) const 
{
    map<string, double>::const_iterator it;
    it = flottants_doubles.find(s);

    if (it != flottants_doubles.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

bool Var_man::GetBoolValue(const string &s, bool & ret_value) const
{
    map<string, bool>::const_iterator it;
    it = booleens.find(s);

    if (it != booleens.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

bool Var_man::GetStringValue(const string &name, string & ret_value) const
{
    map<string, string>::const_iterator it;
    it = chaines.find(name);

    if (it != chaines.end())
    {
        ret_value = it->second;

        return true;
    }

    return false;
}

string Var_man::GetSeqAffect() const
{
	return seq_affect;
}

/*****************************************************************************
* Mutateurs                                                                  *
*****************************************************************************/

void Var_man::SetSeqAffect(const string &s)
{
	seq_affect=s;
}

/*****************************************************************************
* Expanseurs : ajoutent un element                                           *
*****************************************************************************/

bool Var_man::AddInt(const string &s, const int &value)
{
    if (Is(s))
    {
        return false;
    }
    else
    {
        entiers[s] = value;

        return true;
    }
}

bool Var_man::AddSize_t(const string &s, const size_t &value)
{
    if (Is(s))
    {
        return false;
    }
    else
    {
        type_taille[s] = value;

        return true;
    }
}
bool Var_man::AddFloat(const string &s, const float &value)
{
    if (Is(s))
    {
        return false;
    }
    else
    {
        flottants[s] = value;

        return true;
    }
}

bool Var_man::AddDouble(const string &s, const double &value)
{
    if (Is(s))
    {
        return false;
    }
    else
    {
        flottants_doubles[s] = value;

        return true;
    }
}

bool Var_man::AddBool(const string &s, const bool &value)
{
    if (Is(s))
    {
        return false;
    }
    else
    {
        booleens[s] = value;

        return true;
    }
}

bool Var_man::AddString(const string &name, const string &value)
{
    if (IsAvailable(name))
    {
        chaines[name] = value;

        return true;
    }
    else
    {
        return false;
    }
}

bool Var_man::AddString(const string & name, const char * value)
{
	return AddString(name, (string) value);
}

/*****************************************************************************
* Modifyers                                                                  *
*****************************************************************************/

bool Var_man::SetVar(const string &s, const int &value)
{
    if (IsInt(s))
    {
        entiers[s]=value;
        
        return true;
    }
    else
        return false;
}

bool Var_man::SetVar(const string &s, const size_t &value)
{
	if (IsSize_t(s))
	{
		type_taille[s]=value;
		
		return true;
	}
	else
		return false;
}

bool Var_man::SetVar(const string &s, const float &value)
{
	if (IsFloat(s))
	{
		flottants[s]=value;
		
		return true;
	}
	else
		return false;
}

bool Var_man::SetVar(const string &s, const double &value)
{
	if (IsDouble(s))
	{
		flottants_doubles[s]=value;
		
		return true;
	}
	else
		return false;
}

bool Var_man::SetVar(const string &s, const bool &value)
{
	if (IsBool(s))
	{
		booleens[s]=value;
		
		return true;
	}
	else
		return false;
}

bool Var_man::SetVar(const string &s, const string &value)
{
	if (IsString(s))
	{
		chaines[s]=value;
		
		return true;
	}
	else
		return false;
}

/*****************************************************************************
* Accesseurs d'etat : retourne true si l'element appartient a la liste       *
*****************************************************************************/

bool Var_man::IsInt(const string &name) const 
{
    return ((entiers.count(name)==0) ? false : true);
}

bool Var_man::IsSize_t(const string &name) const
{
    return ((type_taille.count(name)==0) ? false : true);
}

bool Var_man::IsFloat(const string &name) const
{
    return ((flottants.count(name)==0) ? false : true);
}

bool Var_man::IsDouble(const string &name) const 
{
    return ((flottants_doubles.count(name)==0) ? false : true);
}

bool Var_man::IsBool(const string &name) const
{
    return ((booleens.count(name)==0) ? false : true);
}

bool Var_man::IsString(const string &name) const
{
    return ((chaines.count(name)==0) ? false : true);
}

/*****************************************************************************
* bool handle(const string argt)                                             *
* Modifie les valeurs des variables existantes : n'ajoute pas de variable a  * 
* la liste                                                                   *
* retourne false si la syntaxe est incorrecte ou si un nom inconnu apparait  *
*****************************************************************************/

bool Var_man::handle(const string &argt)
{
    size_t wpos;

    wpos = argt.find_first_of(' ');

    if (wpos != string::npos)
    {
        string temp1 = argt.substr(0, wpos);
        string temp2 = argt.substr(wpos + 1);

        return handle(temp1) & handle(temp2);
    }

    size_t pos;
    pos = argt.find_first_of(seq_affect);

    if (pos == string::npos)
    {
        return false;
    }

    string temp = argt.substr(0, pos);
    string val_temp = argt.substr(pos + 1);

    if (IsInt(temp))
    {
        int val;
        (stringstream) val_temp >> dec >> val;
        entiers[temp] = val;
        return true;
    }

    if (IsSize_t(temp))
    {
        size_t val;
        (stringstream) val_temp >> dec >> val;
        type_taille[temp] = val;
        return true;
    }
    
    if (IsFloat(temp))
    {
        float val;
        (stringstream) val_temp >> val;
        flottants[temp] = val;
        return true;
    }

    if (IsDouble(temp))
    {
        double val;
        (stringstream) val_temp >> val;
        flottants_doubles[temp] = val;
        return true;
    }

    if (IsBool(temp))
    {
        booleens[temp] = (val_temp == "true" ? true : false);
        return true;
    }

    if (IsString(temp))
    {
        chaines[temp] = val_temp;
        return true;
    }

    return false;
}

/*****************************************************************************
* void Display_all()                                                         *
* Affiche tous les arguments                                                 *
*****************************************************************************/

string Var_man::Display_all(bool value) const
{
	stringstream s;
	string ret;
	
    	if (!entiers.empty())
    	{
        	map<string, int>::const_iterator it;

       		for (it = entiers.begin() ; it != entiers.end() ; it ++)
        	{
            		s << it->first;
            		if (value) s << seq_affect << it->second;
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}

    	if (!type_taille.empty())
    	{
        	map<string, size_t>::const_iterator it;

        	for (it = type_taille.begin() ; it != type_taille.end() ; it ++)
        	{
            		s << it->first;
            		if (value) s << seq_affect << it->second;
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}

    	if (!flottants.empty())
    	{
        	map<string, float>::const_iterator it;

        	for (it = flottants.begin() ; it != flottants.end() ; it ++)
        	{
            		s << it->first;
            		if (value) s << seq_affect << it->second;
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}

    	if (!flottants_doubles.empty())
    	{
        	map<string, double>::const_iterator it;

        	for (it = flottants_doubles.begin() ; it != flottants_doubles.end() ; it ++)
        	{
            		s << it->first;
            		if (value) s << seq_affect << it->second;
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}

    	if (!booleens.empty())
    	{
        	map<string, bool>::const_iterator it;

        	for (it = booleens.begin() ; it != booleens.end() ; it ++)
        	{	
            		s << it->first;
            		if (value) s << seq_affect << (it->second == true ? "true" : "false");
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}

    	if (!chaines.empty())
    	{
        	map<string, string>::const_iterator it;

        	for (it = chaines.begin() ; it != chaines.end() ; it ++)
        	{
            		s << it->first;
            		if (value) s << seq_affect << it->second;
            		ret = ret + s.str() + " ";
            		s.str(string());
        	}
    	}
    	 	
    	return ret;
}

/*****************************************************************************
* Methodes generiques                                                        *
*****************************************************************************/

bool Var_man::Get(const string &s, int & ret_value) const
{
    return GetIntValue(s, ret_value);
}

bool Var_man::Get(const string &s, size_t & ret_value) const
{
    return GetSize_tValue(s, ret_value);
}

bool Var_man::Get(const string &s, float & ret_value) const
{
    return GetFloatValue(s, ret_value);
}

bool Var_man::Get(const string &s, double & ret_value) const
{
    return GetDoubleValue(s, ret_value);
}

bool Var_man::Get(const string &s, bool & ret_value) const
{
    return GetBoolValue(s, ret_value);
}

bool Var_man::Get(const string &s, string & ret_value) const
{
    return GetStringValue(s, ret_value);
}

/*************************************************************/

bool Var_man::Add(const string &s, const int &value)
{
    return AddInt(s, value);
}

bool Var_man::Add(const string &s, const size_t &value)
{
    return AddSize_t(s, value);
}

bool Var_man::Add(const string &s, const float &value)
{
    return AddFloat(s, value);
}

bool Var_man::Add(const string &s, const double &value)
{
    return AddDouble(s, value);
}

bool Var_man::Add(const string &s, const bool &value)
{
    return AddBool(s, value);
}

bool Var_man::Add(const string &s, const string &value)
{
    return AddString(s, value);
}

bool Var_man::Add(const string &s, const char * value)
{
	return AddString(s, value);
}

/*************************************************************/

bool Var_man::Is(const string &s) const
{
    return IsInt(s) | IsSize_t(s) | IsDouble(s) | IsFloat(s) | IsBool(s) | IsString(s);
}

bool Var_man::IsAvailable(const string &s) const
{
	return !Is(s);
}

/*****************************************************************************
* bool Display(const string name)                                            *
* Affiche la variable name                                                   *
*****************************************************************************/

bool Var_man::Display(const string &name, string &ret) const
{
    int i;
    stringstream s;

    if (Get(name, i))
    {
        s << name << seq_affect << i << endl;
        s >> ret;
        return true;
    }

    size_t st;
    if (Get(name, st))
    {
        s << name << seq_affect << st << endl;
	s >> ret;
        return true;
    }

    float f;
    if (Get(name, f))
    {
        s << name << seq_affect << f << endl;
	s >> ret;
        return true;
    }

    double d;
    if (Get(name, d))
    {
        s << name << seq_affect << d << endl;
	s >> ret;
        return true;
    }

    bool b;
    if (Get(name, b))
    {
        s << name << seq_affect << (b ? "true" : "false") << endl;
        s >> ret;
        return true;
    }

    string str;
    if (Get(name, str))
    {
        s << name << seq_affect << str << endl;
        s >> ret;
        return true;
    }

    return false;
}

/*****************************************************************************
* bool GetXML(SimpleXMLTree &, string name)                                  *
* Add the value and type of parameter name to the argument XML Tree          *
*****************************************************************************/

bool Var_man::GetIntXML(SimpleXMLTree & ret, const string & name) const
{
    int i;
    stringstream s;
    string text;
    
    if (Get(name, i))
    {
        s << i << endl;
        s >> text;
        
        ret.Append(new SimpleXMLTree(name, text, 0)); 
        
        return true;
    }
    
    return false;
}	

bool Var_man::GetSize_tXML(SimpleXMLTree & ret, const string & name) const 
{
	stringstream s;
	size_t st;
	string text;
    	
    	if (Get(name, st))
    	{
    	    s << st << endl;
    	    s >> text;  	    
        
    	    ret.Append(new SimpleXMLTree(name, text, 0)); 

    	    return true;
    	}
    	
    	return false;
}

bool Var_man::GetFloatXML(SimpleXMLTree & ret, const string & name) const
{
	stringstream s;
	string text;
	float f;

	if (Get(name, f))
	{
		s << f << endl;
		s >> text;
		
		ret.Append(new SimpleXMLTree(name, text, 0)); 
		
		return true;
	}
	
	return false;
}

bool Var_man::GetDoubleXML(SimpleXMLTree & ret, const string & name) const
{
	stringstream s;
	string text;
	double d;
	
	if (Get(name, d))
	{
		s << d << endl;
		s >> text;
		
		ret.Append(new SimpleXMLTree(name, text, 0)); 
		
		return true;
	}
	
	return false;
}


bool Var_man::GetBoolXML(SimpleXMLTree & ret, const string & name) const
{
	stringstream s;
	string text;
	bool b;

	if (Get(name, b))
	{
		s << (b ? "true" : "false") << endl;
		s >> text;
		
        ret.Append(new SimpleXMLTree(name, text, 0));
		
		return true;
	}
	
	return false;
}

bool Var_man::GetStringXML(SimpleXMLTree & ret, const string & name) const
{
	string text;
   	string str;

	if (Get(name, str))
	{
		text=str;
				
		ret.Append(new SimpleXMLTree(name, text, 0)); 

		return true;
	}
	return false;
}

bool Var_man::GetVarXML(SimpleXMLTree & ret, const string & name) const
{
    if (IsInt(name))
    {
    	return GetIntXML(ret, name);
    }

    if (IsSize_t(name))
    {
        return GetSize_tXML(ret, name);
    }

    if (IsFloat(name))
    {
    	return GetFloatXML(ret, name);
    }

    if (IsDouble(name))
    {
	return GetDoubleXML(ret, name);
    }

    if (IsBool(name))
    {
	return GetBoolXML(ret, name);
    }

    if (IsString(name))
    {
	return GetStringXML(ret, name);
    }

    return false;
}

/*****************************************************************************
* bool GetXML(SimpleXMLTree &, string name)                                  *
* Add all the values of the container into an existing XML Tree              *
*****************************************************************************/

void Var_man::GetVarXML(SimpleXMLTree & ret) const
{
	size_t nelem=0;
	
	nelem+=entiers.size();
	nelem+=type_taille.size();
	nelem+=flottants.size();
	nelem+=flottants_doubles.size();
	nelem+=booleens.size();
	nelem+=chaines.size();
			
	ret.MakeRoom(nelem);
	
    	if (!entiers.empty())
    	{
        	map<string, int>::const_iterator it;

       		for (it = entiers.begin() ; it != entiers.end() ; it ++)
        	{
			GetIntXML(ret, it->first);
        	}
    	}

    	if (!type_taille.empty())
    	{
        	map<string, size_t>::const_iterator it;

        	for (it = type_taille.begin() ; it != type_taille.end() ; it ++)
        	{
			GetSize_tXML(ret, it->first);
        	}
    	}

    	if (!flottants.empty())
    	{
        	map<string, float>::const_iterator it;

        	for (it = flottants.begin() ; it != flottants.end() ; it ++)
        	{
			GetFloatXML(ret, it->first);
		}
    	}

    	if (!flottants_doubles.empty())
    	{
        	map<string, double>::const_iterator it;

        	for (it = flottants_doubles.begin() ; it != flottants_doubles.end() ; it ++)
        	{
			GetDoubleXML(ret, it->first);
        	}
    	}

    	if (!booleens.empty())
    	{
        	map<string, bool>::const_iterator it;

        	for (it = booleens.begin() ; it != booleens.end() ; it ++)
        	{	
			GetBoolXML(ret, it->first);
        	}
    	}

    	if (!chaines.empty())
    	{
        	map<string, string>::const_iterator it;

        	for (it = chaines.begin() ; it != chaines.end() ; it ++)
        	{
			GetStringXML(ret, it->first);
	       	}
    	}
}



