/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/****************************************************************************************************************************************************
* Classe generique pour la gestion de commandes passees en ligne.                                                                                   *
* Les parametres les plus courants d'arguments sont pris en compte.                                                                                 *
* La classe herite de Var_man.                                                                                                                      *
****************************************************************************************************************************************************/

#ifndef __CMD_MAN_H__
	#define __CMD_MAN_H__
	
#include <string>
#include <iostream>
#include <fstream>
#include <map>

#include "Var_man.h"

class Cmd_man:public Var_man
{
	private:
		map<string, bool> Commandes;
		map<string, string> Description;
		
		string prefix;
		
	public:
		Cmd_man(string seq_aff="=", string pre="--");
		
virtual		~Cmd_man() {};
	// Declare une commande a recuperer
	void	Set(const string & name, const string & help="", bool val=false);
	// Modifie la valeur d'une commande et la declare si besoin
	void 	SetVal(const string & name, bool val);
	// Ajoute une ligne dans le fichier d'aide
	void	SetHelp(const string & name, const string & help);
	
	// Recupere la valeur d'une commande
	bool	GetCmd(const string & name, bool & val) const;
	
	// Analyse une chaine de type ligne de commande
	bool	handle(const string &);
	
	// Affiche l'aide
	void	Display_help(ostream &, bool values=false) const;
	
	// appends an XML version of the command name to a tree
	bool	GetCmdXML(SimpleXMLTree &, string name) const;
	
	// appends an XML version of the commands to a tree
	bool 	GetCmdXML(SimpleXMLTree &) const;
	
friend ostream&	operator<< (ostream & out, Cmd_man c)
{
	c.Display_help(out);
	out << "Valeurs : " << c.Display_all(true) << endl;
	return out;
}

friend istream& operator>> (istream & in, Cmd_man c)
{
	string temp;

	in >> temp;
	
	if (in.good())
		c.handle(temp);

	return in;
}

};

#endif

