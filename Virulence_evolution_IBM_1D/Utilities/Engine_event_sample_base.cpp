/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_event_sample_base.cpp                                               *
* Classe de base pour un moteur de gestion des evenements                    *
* Preconfiguration d'enregistrements reguliers                               *
*****************************************************************************/

#include "Engine_event_sample_base.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Engine_event_sample_base::Engine_event_sample_base(double se, bool sae, string ef, bool fc, bool h, bool v)
	:Engine_event_base(ef, fc, h, v), sample_every(se), sample_at_exit(sae)
{
	// Frequence d'echantillonnage (0 : pas d'echantillonnage)
	Add("sample_every", sample_every);
	SetHelp("sample_every", "Periode d'echantillonnage (0 : pas d'echantillonnage)");
	
	// Echantillonnage a la fin du programme
	Set("--sample_at_exit", "Echantillonner a la fin du programme", sample_at_exit);
}


/*****************************************************************************
* Echantillonnage (initialisation)                                           *
*****************************************************************************/

bool Engine_event_sample_base::SampleInit()
{
	return true;
}

/*****************************************************************************
* Echantillonnage (test)                                                     *
*****************************************************************************/
	
bool Engine_event_sample_base::Sample()
{
	cout << evman.GetTime() << endl;
	return true;
}

/*****************************************************************************
* Echantillonnage final (par defaut : echantillonnage normal)                *
*****************************************************************************/

bool Engine_event_sample_base::SampleAtExit()
{
	return Sample();
}

/*****************************************************************************
* Gere les evenements (test)                                                 *
*****************************************************************************/

bool Engine_event_sample_base::HandleEvent(size_t ev)
{
	return true;
}

