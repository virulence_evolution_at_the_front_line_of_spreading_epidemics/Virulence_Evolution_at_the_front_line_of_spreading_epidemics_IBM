/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_base.h                                                              *
* Base class for a programe engine                                           *
*****************************************************************************/

#ifndef __ENGINE_BASE_H__
	#define __ENGINE_BASE_H__
	
#include <iostream>

using namespace std;

#include "User_interaction/Cmd_man.h"

// Cmd_man deals with command-line arguments       
class Engine_base:public Cmd_man 
{
	private:
		bool help, verbose;
		
	protected:
virtual 	int Internal_loop_0()=0; // Main loop (heritable)
		
	public:
		Engine_base(bool h=false, bool v=false);
virtual		~Engine_base() {};
		
inline		bool Help() const { return help; };
inline		bool Verbose() const { return verbose; };
		
		bool Start(size_t argc, const char * argv[]); // Argument analyse
		int Loop(); // Main loop
};

#endif

