/*

    This program does IBM simulations for an epidemiological model
    
    Virulence_evolution_IBM_1D Copyright (C) 2015 Quentin Griette
    
    This file is part of Virulence_evolution_IBM_1D.

    Virulence_evolution_IBM_1D is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Virulence_evolution_IBM_1D is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Virulence_evolution_IBM_1D.  If not, see <http://www.gnu.org/licenses/>.
    
    Contact : Quentin Griette <q.griette@gmail.com>

*/

/*****************************************************************************
* Engine_event_XMLsample_base.cpp                                            *
* Classe de base pour un moteur de gestion des evenements                    *
* Preconfiguration d'enregistrements reguliers                               *
* Gestion basique de XML                                                     *
*****************************************************************************/

#include "Engine_event_XMLsample_base.h"

/*****************************************************************************
* Constructeurs                                                              *
*****************************************************************************/

Engine_event_XMLsample_base::Engine_event_XMLsample_base(double se, const string & so, bool sae, bool s, bool nxd, const string & ef, bool fc, bool h, bool v, string kou, string csv_field_sep, string csv_rec_sep, bool sa)
	:Engine_event_base(ef, fc, h, v), sample_every(se), sample_out(so), sample(s), sample_at_exit(sae), no_declare_XML(nxd), evil_print(false), kind_of_output(kou), csv_field_separator(csv_field_sep), csv_record_separator(csv_rec_sep), sample_template("sample"), sample_toprint(NULL), indent(""), inc("  "), nsamples(0), sample_after(sa)
{
	// Frequence d'echantillonnage (0 : pas d'echantillonnage)
	Add("sample_every", sample_every);
	Add("sample_out", sample_out);
	Add("kind_of_output", kind_of_output);
	Add("csv_field_separator", csv_field_separator);
	Add("csv_record_separator", csv_record_separator);
	SetHelp("sample_every", "Time between two samples");
	SetHelp("sample_out", "File to store the samples");
	
	// Echantillonnage a la fin du programme
	Set("--sample", "Whether to sample", sample);
	Set("--sample_at_exit", "Sample when EVENT_EXIT is received", sample_at_exit);
	Set("--no_declare_XML", "Don't print the initial XML declaration", no_declare_XML);
	Set("--evil_print", "Print less human-readable but more space-efficient XML", evil_print);
}


/*****************************************************************************
* Print : print a formatted sample                                           *
*****************************************************************************/

void Engine_event_XMLsample_base::Print(bool force_xml, bool first_time, bool close) 
{ 
	ostream * out=&cout; 
	ofstream fout;
	
	if (sample_out.size()>0)
	{
		if (first_time)
		{
			fout.open(sample_out.c_str(), fstream::out | fstream::trunc);
		}
		else
		{
			fout.open(sample_out.c_str(), fstream::out | fstream::app);
		}
		
		if (fout.is_open())
		{
			out=&fout;
		}
		else
		{
			cerr << "Unable to open file : " << sample_out << endl;
		}
	}
	
	// Initializes the output
	if (first_time)
	{
		// Declares a XML sheet if not requested not to
		if (!no_declare_XML)
		{
			sample_toprint->XMLdeclare(*out);
		}
		
		// If data saving is required, don't print the EOL
		if (evil_print)
		{
			*out << "<root>";
		}
		else
		{
			*out << "<root>" << endl;
		}
		
		indent=inc;
	}
	
	// Constructs the XML CSV structure declaration
	if (first_time && (kind_of_output==OUTPUT_SAMPLE_AS_CSV))
	{
		SimpleXMLTree * xml_struct=new SimpleXMLTree("structure", "", sample_template.GetSize());
		
		xml_struct->Append(new SimpleXMLTree("FS", csv_field_separator, 0));
		xml_struct->Append(new SimpleXMLTree("RS", csv_record_separator, 0));
		size_t j=0;
		
		for (size_t i=0; i<sample_template.GetSize(); i++)
		{
			SimpleXMLTree * template_child=NULL;
		
			template_child=sample_template.GetChild(i);
		
			if (template_child != NULL)
			{
				stringstream number;
				number << j++;
				xml_struct->Append(new SimpleXMLTree("field", number.str() + csv_field_separator + template_child->GetTag()));
			}
		}
		
		sample_toprint->Append(xml_struct);				
	}	
	
	// prints the sample as XML if requested to
	if (force_xml || (kind_of_output==OUTPUT_SAMPLE_AS_XML))
	{
		if (evil_print)
		{
			*out << *sample_toprint;
		}
		else
		{
			sample_toprint->NicePrint(*out, indent, inc);
		}
	}
	
	// Begins the csv node if required
	
	if (first_time && (kind_of_output==OUTPUT_SAMPLE_AS_CSV))
	{
		if (evil_print)
		{
			*out << "<samples>";
		}
		else
		{
			*out << indent << "<samples>" << endl;
		}
	}

	// prints the sample in the CSV format
	if ((kind_of_output==OUTPUT_SAMPLE_AS_CSV) && (!force_xml))
	{
		for (size_t i=0; i<sample_toprint->GetSize(); i++)
		{
			SimpleXMLTree * tmp_child=NULL;
			tmp_child=sample_toprint->GetChild(i);
			if (tmp_child != NULL)
			{
				*out << tmp_child->GetText() << csv_field_separator;
			}
		}
		*out << csv_record_separator;
	}
	
	// Ends the output if requested to
	if (close)
	{
		if (kind_of_output==OUTPUT_SAMPLE_AS_CSV)
		{
			*out << "</samples>" << (evil_print ? "":"\n");
		}
		*out << "</root>" << endl;
	}
	
	// free the file buffer
	if (fout.is_open())
	{
		fout.close();
	}
}

/*****************************************************************************
* Echantillonnage (initialisation)                                           *
*****************************************************************************/


/*****************************************************************************
* Echantillonnage (test)                                                     *
*****************************************************************************/


/*****************************************************************************
* Echantillonnage final (par defaut : echantillonnage normal)                *
*****************************************************************************/


/*****************************************************************************
* Gere les evenements (test)                                                 *
*****************************************************************************/


